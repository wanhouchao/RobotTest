import platform
import time
# import win32api
# import win32con
from selenium.common.exceptions import WebDriverException, InvalidArgumentException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait  # 等待库
from selenium.webdriver.support import expected_conditions as ec  # 等待的条件
from selenium.webdriver.common.by import By  # 元素查找方式精简的写法
# from EpsitRobotTest.Comm.Mysqldb import *
from selenium import webdriver
from Comm.BaseFrame.Base import getNewChromeDriver
from Comm.BaseFrame.Log import MyLog
from selenium.webdriver.chrome.options import Options
from Config.BaseConfig.BaseConfig import driverPath


class SeleniumFramework:
    def __init__(self, browserType='web', browserName='ge', deviceName='iPhone X', browserPattern=True,
                 executablePath=driverPath):  # 魔法构建 传入参数
        """
        选择浏览器,默认谷歌浏览器，默认iPhone X
        :param browserType: 浏览器类型 web还是wap
        :param browserName: 浏览器名字google Firefox ie 默认是google
        :param deviceName: 设备名称 wap专用 默认是iPhone X
        :param browserPattern: 浏览器运行模式 默认是前台运行
        """
        # Clear_environment()  # 清理测试环境
        if browserType == 'web':
            if browserName in ['google', 'ge', 'chrome']:
                # 判断浏览器运行模式
                if browserPattern:
                    # 前台运行模式
                    try:
                        chrome_options = Options()
                        chrome_options.add_argument(r"--user-data-dir=E:\\")
                        self.driver = webdriver.Chrome(options=chrome_options, executable_path=executablePath)
                    except WebDriverException:
                        print("正在下载最新版本ChromeDriver")
                        MyLog.info("正在下载最新版本ChromeDriver")
                        getNewChromeDriver()
                        MyLog.info("下载最新版本ChromeDriver成功")
                        self.driver = webdriver.Chrome(executable_path=executablePath)
                    self.driver.maximize_window()  # win最大化
                else:
                    # 设置后台运行模式
                    chrome_options = Options()
                    chrome_options.add_argument('--headless')
                    chrome_options.add_argument('--disable-gpu')
                    chrome_options.add_argument('--no-sandbox')
                    try:
                        self.driver = webdriver.Chrome(options=chrome_options, executable_path=executablePath)
                    except WebDriverException:
                        print("正在下载最新版本ChromeDriver")
                        MyLog.info("正在下载最新版本ChromeDriver-->{}".format(str(WebDriverException)))
                        getNewChromeDriver()
                        self.driver = webdriver.Chrome(executable_path=executablePath)
            elif browserName in ['ff', 'Firefox', 'firefox']:
                self.driver = webdriver.Firefox()
            elif browserName in ['Ie', 'ie']:
                self.driver = webdriver.Ie()
            else:
                self.driver = webdriver.Chrome()
        elif browserType == 'wap':
            self.options = webdriver.ChromeOptions()
            mobileEmulation = {'deviceName': deviceName}
            self.options.add_experimental_option('mobileEmulation', mobileEmulation)
            try:
                self.driver = webdriver.Chrome(options=self.options, executable_path=executablePath)
            except WebDriverException:
                print("正在下载最新版本ChromeDriver")
                getNewChromeDriver()
                self.driver = webdriver.Chrome(options=self.options, executable_path=executablePath)
        else:
            MyLog.error('Type只支持web和wap方法')
            raise print("Type只支持web和wap方法")

    def openUrl(self, url):
        """
        打开url
        :param url: 网页地址
        :return: 成功打开网页 失败停止
        """
        self.driver.get(url)  # 打开url
        self.driver.implicitly_wait(5)  # 智能等待5s
        MyLog.debug('打开:%s' % url)

    def addCookies(self, cookies):
        """
        添加cookie
        :param cookies:必须是列表列表里面的元素必须是字典,--》 [{'name' : 'foo', 'value' : 'bar'}]
        字典里面必须带有name和value {'name': 'access_token_develop', 'value': response["data"]["access_token"]}
        :return:
        """
        self.driver.delete_all_cookies()
        for cookie in cookies:
            try:
                self.driver.add_cookie(cookie)
                MyLog.info("添加cookie-->{}".format(str(cookie)))
            except InvalidArgumentException as exceptionMsg:
                MyLog.error("添加cookie报错，缺少参数-->{}".format(str(exceptionMsg)))
                raise print("添加cookie报错，缺少参数-->{}".format(str(exceptionMsg)))

    def __Wite_element(self, string_ele, timeout=10):
        """增加框架稳定性  每次操作元素之前都先智能等待一下元素在页面上是否可见
            time是等待时间默认10s
        """
        if string_ele.find('=') == -1:
            MyLog.error('元素的方式错误，无法解析！')
            raise print('元素的方式错误，无法解析！')  # raise手动触发异常
        else:
            # t1 = time.time()
            mode = string_ele.split("=", 1)[0]  # 定位方式
            element = string_ele.split("=", 1)[1]  # 定位值
            if mode.lower() == 'id':
                # WebDriverWait(self.驱动器, 等待时间10).until(EC.visibility_of_element_located可((By.ID, element)))
                # 期望检查一个元素是否存在于页面的dom上，并且是可见的。visibility_of_element_located
                WebDriverWait(self.driver, timeout).until(
                    ec.visibility_of_element_located((By.ID, element)))  # 默认轮询0.5秒
            elif mode.lower() == 'name':
                WebDriverWait(self.driver, timeout).until(ec.visibility_of_element_located((By.NAME, element)))
            elif mode.lower() == 'css':
                WebDriverWait(self.driver, timeout).until(ec.visibility_of_element_located((By.CSS_SELECTOR, element)))
            elif mode.lower() == 'xpath':
                WebDriverWait(self.driver, timeout).until(ec.visibility_of_element_located((By.XPATH, element)))
            elif mode.lower() == 'class':
                WebDriverWait(self.driver, timeout).until(ec.visibility_of_element_located((By.CLASS_NAME, element)))
            elif mode.lower() == 'text':
                WebDriverWait(self.driver, timeout).until(ec.visibility_of_element_located((By.LINK_TEXT, element)))
            else:
                MyLog.error('元素的方式错误，无法解析！')
                raise print('元素的方式错误，无法解析')
            # print("%s 元素等待%s 秒！" % (string_ele, time.time() - t1))

    def __findElement(self, string_ele):
        """
        多元素查找返回元素列表
        分析传入字符串传到对应的方式里面去,找元素
        例如：id name xpath css  classname text
        传入形式如：id=user name=pwd xpath=/html/body/div css=input[type="password"]
        :param string_ele: 传入ele定位方式
        :return: 成功返回element 失败停止运行
        """
        # 处理传入的字符串 按照=号切割
        # time.sleep(1)
        self.__Wite_element(string_ele)
        ele_list = str(string_ele).split('=', 1)  # 按照第一个=号切割 例如：['xpyth','/hrnl/body/div']
        # print(ele_list[0])
        MyLog.debug('使用 %s 查找 %s元素' % (ele_list[0], ele_list[1]))  # 记录日志
        if ele_list[0] == 'id':
            ele = self.driver.find_element_by_id(ele_list[1])
            self.driver.execute_script("arguments[0].scrollIntoView(false);", ele)
            return ele
        elif ele_list[0].lower() == 'name':
            ele = self.driver.find_element_by_name(ele_list[1])
            self.driver.execute_script("arguments[0].scrollIntoView(false);", ele)
            return ele
        elif ele_list[0].lower() == 'css':
            ele = self.driver.find_element_by_css_selector(ele_list[1])
            self.driver.execute_script("arguments[0].scrollIntoView(false);", ele)
            return ele
        elif ele_list[0].lower() == 'classname':
            ele = self.driver.find_element_by_class_name(ele_list[1])
            self.driver.execute_script("arguments[0].scrollIntoView(false);", ele)
            return ele
        elif ele_list[0].lower() == 'xpath':
            ele = self.driver.find_element_by_xpath(ele_list[1])
            self.driver.execute_script("arguments[0].scrollIntoView(false);", ele)
            return ele
        elif ele_list[0].lower() == 'text':
            ele = self.driver.find_element_by_link_text(ele_list[1])
            self.driver.execute_script("arguments[0].scrollIntoView(false);", ele)
            return ele

    def onClick(self, string_ele):
        """
        点击元素
        :param string_ele:
        :return: 成功返回None 失败停止运行
        """
        ele = self.__findElement(string_ele)
        ele.click()
        MyLog.debug('点击%s元素' % str(string_ele).split('=', 1)[1])

    def doClear(self, string_ele):
        """
        清除输入框中内容
        :param string_ele: 传入ele定位方式
        :return: 成功返回None 失败停止运行
        """
        ele = self.__findElement(string_ele)
        ele.clear()
        MyLog.debug('清除%s输入框内容' % str(string_ele).split('=', 1)[1])

    def doWrite(self, string_ele, value):
        """
        在输入框写入val
        :param string_ele: 传入ele定位方式
        :param value: 写入的值
        :return: None
        """
        ele = self.__findElement(string_ele)
        ele.clear()
        ele.send_keys(value)
        MyLog.debug('输入 %s ' % value)

    def switchToFrame(self, frameId):
        """
        切入框架
        :param frameId: 框架id
        :return: 成功返回None 失败停止运行
        """
        self.driver.switch_to.frame(frameId)
        MyLog.debug('切入框架 %s ' % frameId)

    def getText(self, string_ele):
        """
        提取文本
        :param string_ele: 传入ele定位方式
        :return:成功返回提取的文本  失败停止
        """
        ele = self.__findElement(string_ele)
        MyLog.debug('提取%s文本元素' % str(string_ele).split('=', 1)[1])
        return ele.text

    def rollElementToView(self, string_ele):
        """
        滚动元素到可视位置
        :param string_ele: 传入ele定位方式
        :return: None
        """
        ele = self.__findElement(string_ele)
        self.driver.execute_script("arguments[0].scrollIntoView();", ele)
        MyLog.debug('%s滚动元素到可视位置' % str(string_ele).split('=', 1)[1])

    def exitFrame(self):
        """
        退出框架
        :return:成功返回None 失败停止运行
        """
        self.driver.switch_to.default_content()
        MyLog.debug('退出框架')

    def switchToNewWindow(self):
        """
        切换到最新窗口
        :return: 成功返回None 失败停止运行
        """
        win_list = self.driver.window_handles
        self.driver.switch_to.window(win_list[-1])
        MyLog.debug('切换到最新窗口')

    def closeCurrentWindow(self):
        """
        关闭当前窗口
        :return: 成功返回None 失败停止运行
        """
        self.driver.close()
        MyLog.debug('关闭当前窗口')

    def exitBrowser(self):
        """
        退出浏览器
        :return: 成功返回None 失败停止运行
        """
        self.driver.quit()
        MyLog.debug('退出浏览器')

    def rollTwoArrow(self):
        """
        滚动条向上输入2个arrow_up
        :return: None
        """
        self.driver.find_element_by_xpath('/html/body').send_keys(Keys.ARROW_UP)
        self.driver.find_element_by_xpath('/html/body').send_keys(Keys.ARROW_UP)

    def notSelectBox(self, string_ele_div, string_ele_li):
        """
        处理不是Select下拉框
        :param string_ele_div: 最外层div的定位方式
        :param string_ele_li: 标签的定位方式
        :return: 成功返回None 失败停止运行
        """
        # 找到下拉列表
        self.__Wite_element(string_ele_div)
        ele_div = self.__findElement(string_ele_div)
        self.rollTwoArrow()
        # 点击列表
        ele_div.click()
        # 找到选择的元素
        self.__Wite_element(string_ele_li)
        ele_li = self.__findElement(string_ele_li)
        # 点击元素
        ele_li.click()
        time.sleep(0.5)

    def SelectVisibleText(self, string_ele, text):
        """
        select下拉框处理之文本 按照文本定位
        :param string_ele: 传入ele定位方式
        :param text: 传入标签的文本
        :return: 成功返回None 失败停止运行
        """
        ele = self.__findElement(string_ele)
        Select(ele).select_by_visible_text(text)

    def SelectVisibleIndex(self, string_ele, index):
        """
        select下拉框处理之下标
        :param string_ele: 传入ele定位方式
        :param index: 传入标签的索引
        :return: 成功返回None 失败停止运行
        """
        # ele = self.__findElement(string_ele)
        Select(self.driver.find_element_by_xpath(string_ele)).select_by_index(index)

    def SelectVisibleValue(self, string_ele, value):
        """
        select下拉框处理之之值
        :param value: select的value
        :param string_ele: 传入ele定位方式
        :return: 成功返回None 失败停止运行
        """
        ele = self.__findElement(string_ele)
        Select(ele).select_by_value(value)

    def moveMouseToElement(self, string_ele):
        """
        移动鼠标到元素上
        :param string_ele: 传入ele定位方式
        :return: 成功返回None 失败停止运行
        """
        ele = self.__findElement(string_ele)
        self.rollElementToView(string_ele)
        ActionChains(self.driver).move_to_element(ele).perform()
        MyLog.debug('移动鼠标到元素上')

    def findElement(self, string_ele):
        """
        多元素查找返回元素列表
        分析传入字符串传到对应的方式里面去,找元素
        例如：id name xpath css  classname text
        传入形式如：id=user name=pwd xpath=/html/body/div css=input[type="password"]
        :param string_ele: 传入ele定位方式
        :return: 成功返回element 失败停止运行
        """
        # 处理传入的字符串 按照=号切割
        # time.sleep(1)
        if str(string_ele).find('=') == -1:
            MyLog.error('元素输入错误，找不到元素')
            raise print('元素输入错误，找不到元素')  # raise 手动出发异常
        else:
            ele_list = str(string_ele).split('=', 1)  # 按照第一个=号切割 例如：['xpyth','/hrnl/body/div']
            # print(ele_list[0])
            if ele_list[0] == 'id':
                return self.driver.find_element_by_id(ele_list[1])
            elif ele_list[0].lower() == 'name':
                return self.driver.find_element_by_name(ele_list[1])
            elif ele_list[0].lower() == 'css':
                return self.driver.find_element_by_css_selector(ele_list[1])
            elif ele_list[0].lower() == 'classname':
                return self.driver.find_element_by_class_name(ele_list[1])
            elif ele_list[0].lower() == 'xpath':
                return self.driver.find_element_by_xpath(ele_list[1])
            elif ele_list[0].lower() == 'text':
                return self.driver.find_element_by_link_text(ele_list[1])
            else:
                MyLog.error('找不到这个元素')
                raise print('找不到这个元素')

    def findElements(self, string_ele):
        """
        多元素查找返回元素列表
        分析传入字符串传到对应的方式里面去,找元素
        例如：id name xpath css  classname text
        传入形式如：id=user name=pwd xpath=/html/body/div css=input[type="password"]
        :param string_ele: 传入ele定位方式
        :return: 成功返回element 失败停止运行
        """
        # 处理传入的字符串 按照=号切割
        # time.sleep(1)
        if str(string_ele).find('=') == -1:
            MyLog.error('元素输入错误，找不到元素')
            raise print('元素输入错误，找不到元素')  # raise 手动出发异常
        else:
            ele_list = str(string_ele).split('=', 1)  # 按照第一个=号切割 例如：['xpyth','/hrnl/body/div']
            # print(ele_list[0])
            if ele_list[0] == 'id':
                return self.driver.find_elements_by_id(ele_list[1])
            elif ele_list[0].lower() == 'name':
                return self.driver.find_elements_by_name(ele_list[1])
            elif ele_list[0].lower() == 'css':
                return self.driver.find_elements_by_css_selector(ele_list[1])
            elif ele_list[0].lower() == 'classname':
                return self.driver.find_elements_by_class_name(ele_list[1])
            elif ele_list[0].lower() == 'xpath':
                return self.driver.find_elements_by_xpath(ele_list[1])
            elif ele_list[0].lower() == 'text':
                return self.driver.find_elements_by_link_text(ele_list[1])
            else:
                MyLog.error('找不到这个元素')
                raise print('找不到这个元素')

    def submit(self, string_ele):
        """
        提交表单
        :param string_ele:  传入ele定位方式
        :return: 成功返回None
        """
        ele = self.__findElement(string_ele)
        ele.submit()

    # def uploadFile(self, string_ele, filePath):
    #     """
    #     使用 python 的 win32api，win32con 模拟按键输入，实现文件上传操作。
    #     :param string_ele: 传入ele定位方式
    #     :param filePath: 要上传的文件地址，绝对路径。如：D:\\time (1).jpg
    #     :return: 成功返回：上传文件后的地址，失败返回：""
    #     """
    #     pyperclip.copy(filePath)  # 复制文件路径到剪切板
    #     self.onClick(string_ele)  # 点击上传图片按钮
    #     time.sleep(3)  # 等待程序加载 时间 看你电脑的速度 单位(秒)
    #     # 发送 ctrl（17） + V（86）按钮
    #     win32api.keybd_event(17, 0, 0, 0)
    #     win32api.keybd_event(86, 0, 0, 0)
    #     win32api.keybd_event(86, 0, win32con.KEYEVENTF_KEYUP, 0)  # 松开按键
    #     win32api.keybd_event(17, 0, win32con.KEYEVENTF_KEYUP, 0)
    #     time.sleep(1)
    #     win32api.keybd_event(13, 0, 0, 0)  # (回车)
    #     win32api.keybd_event(13, 0, win32con.KEYEVENTF_KEYUP, 0)
    #     MyLog.debug('%s上传文件%s' % (string_ele, filePath))


if __name__ == '__main__':
    br = SeleniumFramework(browserType="wap", browserPattern=True)
    br.openUrl('https://www.baidu.com/')
    br.doWrite('id=index-kw', '刘俊杰')
    br.driver.execute_script("document.getElementById(\"user-content-requirements\")")
