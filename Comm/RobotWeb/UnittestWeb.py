import time
import unittest
from Comm.BaseFrame.Log import MyLog
from Comm.RobotWeb.SeleniumFramework import SeleniumFramework
from Config.WebConfig.test_epsit import User, password, Robot_url


class unittestWeb(unittest.TestCase):
    """
    这个里面是测试用例添加的公共方法，分离出来方便维护
    这个是web端的公共方法
    """

    def login(self, browserPattern=True):
        """登录方法"""
        self.br = SeleniumFramework(browserPattern=browserPattern)
        self.br.openUrl(Robot_url)
        self.br.doWrite('xpath=//input[@placeholder="用户名"]', User)
        self.br.doWrite('xpath=//input[@placeholder="密码"]', password)
        self.br.onClick('xpath=//button[text()="登录"]')
        MyLog.debug('%s登录：%s' % (User, Robot_url))
        return self.br  # 返回一个浏览器对象给用例接收

    def clickPanel(self):
        """点击面板,返回面板界面"""
        self.br.onClick('xpath=//span[text()="面板"]/../..')

    def epsEnterTheModule(self, functionName, organizationName='自动化测试公司'):
        # 进入功能模块页面
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="%s"]/..' % functionName)
        self.br.onClick('css=span[data-v-6f699979]')
        self.br.onClick('xpath=//span[text()="%s"][@class="ant-select-tree-title"]' % organizationName)
        time.sleep(0.5)

    def epsAdd(self, functionName, fieldName='所属机构', organizationName='自动化测试公司'):
        """进入添加页面，functionName--》门控管理"""
        # 点击功能名称
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="%s"]/..' % functionName)
        # 进入添加编辑页面选择所属机构默认是自动化测试公司
        self.br.onClick('css=button[title="增加"]')
        self.br.onClick('xpath=//label[text()="%s"]/../div/div' % fieldName)
        self.br.onClick('xpath=//li[@class="ivu-select-item"]/span[text()="%s"]' % organizationName)
        time.sleep(0.5)
        MyLog.debug("进入 %s 添加页面" % functionName)

    def goToAddPage(self, functionName):
        """进入功能页面跳转到功能页面的添加"""
        # 进入添加编辑
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="%s"]/..' % functionName)
        self.br.onClick('css=button[title="增加"]')

    def goToPage(self, functionName):
        """进入功能页面"""
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="%s"]/..' % functionName)

    def epsChange(self, functionName, UniqueFields, organizationName='自动化测试公司'):
        """进入编辑修改页面 ，functionName--》门控管理，UniqueFields多条件时请用","隔开
        UniqueFields--》 自动化测试公司,A,Y1
        """
        self.epsEnterTheModule(functionName, organizationName=organizationName)
        FieldList = str(UniqueFields).split(",")  # 获取条件列表
        xpathStr = ''
        for Field in FieldList:
            # //div[text()="自动化测试公司"]/../.. //div[text()="3"]/../.. //div[text()="Y1"]/../.. //div/a[@title="删除"]
            xpathStr = xpathStr + '//div[text()="%s"]/../..' % Field
        ele = self.br.findElements('xpath=%s//div/a[@title="修改"]' % xpathStr)  # 多条件拼接一下字符串
        try:
            ele[1].click()
        except Exception as e:
            MyLog.error(e)
            ele[0].click()
        MyLog.debug("进入 %s 编辑修改页面" % functionName)

    def epsDelete(self, functionName, UniqueFields, organizationName='自动化测试公司'):
        """
        删除functionName中的UniqueFields这个配置，UniqueFields多条件时请用","隔开
        UniqueFields--》 自动化测试公司,A,Y1
        """
        self.epsEnterTheModule(functionName, organizationName=organizationName)
        FieldList = str(UniqueFields).split(",")  # 获取条件列表
        xpathStr = ''
        for Field in FieldList:
            # //div[text()="自动化测试公司"]/../.. //div[text()="3"]/../.. //div[text()="Y1"]/../.. //div/a[@title="删除"]
            xpathStr = xpathStr + '//div[text()="%s"]/../..' % Field
        ele = self.br.findElements('xpath=%s//div/a[@title="删除"]' % xpathStr)  # 多条件拼接一下字符串
        try:
            if len(ele) > 1:
                ele[1].click()
            else:
                ele[0].click()
        except Exception as e:
            MyLog.debug(e)
            ele[0].click()
        # 点击确认
        ele = self.br.findElements('xpath=//span[text()="确定"]')
        try:
            ele[0].click()
        except Exception as e:
            MyLog.debug(e)
            ele[1].click()
        time.sleep(0.5)
        MyLog.debug("删除 %s 中包含 %s 的配置 " % (functionName, UniqueFields))

    def epsOnDelete(self):
        """删除提示，点击确定"""
        try:
            self.br.findElements('css=button[class="ivu-btn ivu-btn-primary ivu-btn-large"]')[0].click()
        except Exception as e:
            MyLog.debug(e)
            self.br.findElements('css=button[class="ivu-btn ivu-btn-primary ivu-btn-large"]')[1].click()

    def epsSelectClick(self, fieldName, labelName):
        """
        fieldName-->字段名，labelName-->标签名字
        """
        self.br.onClick('xpath=//label[text()="{}"]/../div/div'.format(fieldName))
        self.br.onClick(
            'xpath=//label[text()="{}"]/../div/div/div[2]/ul[2]/li[text()="{}"]'.format(fieldName, labelName))
        time.sleep(0.5)

    def epsSelectClickContain(self, fieldName, labelName):
        """
        fieldName-->字段名，labelName-->标签名字
        """
        self.br.onClick('xpath=//label[contains(text(),"{}")]/../div/div'.format(fieldName))
        self.br.onClick(
            'xpath=//label[text()="{}"]/../div/div/div[2]/ul[2]/li[contains(.,"{}")]'.format(fieldName,
                                                                                             labelName))
        time.sleep(0.5)

    def epsSort(self, value, fieldName='排序'):
        """排序方法"""
        time.sleep(0.5)
        self.br.doWrite('xpath=//label[text()="%s"]/../div/div/div[2]/input' % fieldName, value)

    def epsWrite(self, fieldName, value):
        """通过字段名称填写信息 fieldName--》楼层，value--》6"""
        time.sleep(0.5)
        try:
            self.br.doWrite('xpath=//label[text()="%s"]/../div/div/input' % fieldName, value)
        except Exception as e:
            MyLog.debug(e)
            self.br.doWrite('xpath=//label[text()="%s"]/../div/div/textarea' % fieldName, value)

    def epsWriteContain(self, fieldName, value):
        """通过字段名称填写信息 fieldName--》楼层，value--》6"""
        time.sleep(0.5)
        self.br.doWrite('xpath=//label[contains(text(),"%s")]/../div/div/input' % fieldName, value)

    def epsWriteRemark(self, fieldName, value):
        """通过字段名称填写信息 fieldName--》备注信息，value--》自动化测试脚本"""
        time.sleep(0.5)
        self.br.doWrite('xpath=//label[text()="%s"]//..//textarea' % fieldName, value)

    def epsSubmit(self):
        """点击提交按钮"""
        self.br.onClick('xpath=//span[text()="提交"]/..')
        MyLog.debug('点击提交按钮')

    def epsDeleteAll(self, functionName):
        """点击全选删除，functionName--》门控管理"""
        # 进入并且筛选机构为自动化测试公司
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="%s"]/..' % functionName)
        self.br.onClick('css=span[data-v-6f699979]')
        self.br.onClick('xpath=//span[text()="自动化测试公司"][@class="ant-select-tree-title"]')
        time.sleep(0.5)
        # 判断是否有数据
        if len(self.br.findElements('xpath=//div[text()="自动化测试公司"]')) != 0:
            # 扩大每页显示
            self.br.SelectVisibleValue('xpath=//div[@style="display: flex; align-items: center;"]/div[2]/select', '50')
            self.br.findElements('xpath=//div[text()="所属机构"]/../../th/div/label/span/span')[0].click()
            self.br.onClick('css=button[title="删除"]')
            self.br.findElements('css=button[class="ivu-btn ivu-btn-primary ivu-btn-large"]')[0].click()

    def TimeControl(self, fieldName, value):
        """
        用js控制时间输入控件
        fieldName->时间控件字段名称
        value->需要传一个列表进来 有几个时间框就传几个数据
        数据实例： value ="17:00" 一个时间格式
        。。。。
        """

        ele = self.br.findElement('xpath=//label[text()="%s"]/../div/div/input' % fieldName)
        # 框架问题必须先初始化时间框里面的value
        # 点击时间框
        ele.click()
        time.sleep(0.5)
        # 清除时间框数据
        ele.clear()
        time.sleep(0.5)
        # 填写时间数据
        ele.send_keys(value)
        # 收回下拉框
        self.br.findElement('xpath=//label[text()="%s"]' % fieldName).click()

    def doAssert(self, string="xpath=//h2[text()=\"操作成功\"]", ExpectedResult='操作成功'):
        # 添加断言
        time.sleep(1)
        try:
            ActualResult = self.br.getText(string)
        except Exception as e:
            MyLog.debug(e)
            title = self.br.getText('xpath=//div[@class="navTitle"]/span')  # 获取当前页面功能名称
            MyLog.error("断言失败---预期结果：%s，页面标题:%s" % (ExpectedResult, title))
            raise AssertionError("断言失败---预期结果:{},页面标题:".format(ExpectedResult, title))
        print('\n' + "实际结果：" + ActualResult + '\n' + "预期结果：" + ExpectedResult + "\n")
        self.assertEqual(ActualResult, ExpectedResult, '对比结果不一致')


    def clickLevelPositioningElement(self, functionName, action, eleInfo: dict):
        """
        多条件精确查询一个元素，
        functionName --》门控管理，是传功能模块名字
        action --》修改，是表明是什么动作 有删除，修改。。。在按钮title中有
        如：<a href="javascript:" title="修改" class="navbar-toggler">中action=修改
        info是一个字典 里面是筛选条件例如
        {'所属机构': '自动化测试公司', '楼宇': 'A', '机器人类型': 'Y1', '楼层': '3', '排序': '3'}
        """
        # 进入功能模块
        self.epsEnterTheModule(functionName)
        # 获取表头
        theadTitleList = str(self.br.getText('xpath=//thead[@class="has-gutter"]')).split(' ')
        # 定位获取父元素
        parentElements = self.br.findElements('xpath=//tr[@class="el-table__row"]')
        # 通过父元素定位子元素
        for parentElement in parentElements:
            # 获取当前索引
            index = parentElements.index(parentElement)
            # 判断一下索引是否是前面一半（前面一半包含标签信息但是页面会隐藏）
            if index < int((len(parentElements) + 1) / 2):
                daughterElementInfo = {}  # 获取标签按照表头去访问
                daughterElementList = str(parentElement.text).split('\n')  # 切割提取的标签
                for i in range(len(daughterElementList)):
                    daughterElementInfo[theadTitleList[i]] = daughterElementList[i]  # 储存数据
                # 比对字典
                num = 0
                for title in eleInfo:
                    try:
                        if eleInfo[title] == daughterElementInfo[title]:
                            num = num + 1
                    except Exception as e:
                        MyLog.debug(e)
                        pass
                if num == len(eleInfo):
                    try:
                        # 由于框架原因 会定位除两个一样的标签 一个可以点击一个不可以点击
                        # 二次查找，再父节点去寻找符合要求的子节点
                        ele = parentElements[index + int((len(parentElements) + 1) / 2)].find_element_by_css_selector(
                            'a[title="%s"]' % action)
                        ele.click()
                    except Exception as e:
                        MyLog.debug(e)
                        ele = parentElements[index].find_element_by_css_selector('a[title="%s"]' % action)
                        ele.click()

    def epsSearchClick(self, value, placeholder, action='修改'):
        """搜索信息并点击 placeholder是提示里面的条件"""
        self.br.doWrite('xpath=//input[@class="search-input ant-input"][@placeholder="%s"]' % placeholder, value)
        self.br.onClick('xpath=//button[@title="查询"]')
        time.sleep(0.5)
        ele = self.br.findElements('xpath=//a[@title="%s"]' % action)  # 多条件拼接一下字符串
        try:
            ele[1].click()
        except Exception as e:
            MyLog.debug(e)
            ele[0].click()

    def epsClick(self, fieldName, labelName):
        """
        fieldName-->字段名，labelName-->标签名字
        """
        self.br.onClick('xpath=//label[text()="{}"]//..//span[text()="{}"]'.format(fieldName, labelName))

    def epsClickAction(self, UniqueFields, action='修改'):
        """
        根据查找唯一值，然后点击编辑或者修改
        比如角色管理模块，根据角色名称点击编辑或者修改
        """
        ele = self.br.findElements(
            "xpath=//div[text()='{}']//..//..//a[@title='{}']".format(UniqueFields, action))  # UniqueFields  为唯一值
        try:
            ele[1].click()
        except Exception as e:
            MyLog.debug(e)
            ele[0].click()

    def epsExitBrowser(self):
        """
        退出浏览器
        :return: None
        """
        self.br.exitBrowser()

    def demo(self):
        """
        演示专用demo
        :return:
        """
        self.br = SeleniumFramework(browserPattern=True)
        self.br.openUrl("https://www.baidu.com/")
