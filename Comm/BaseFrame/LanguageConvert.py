import opencc
from Config.BaseConfig.LanguageCode import *


class MyOpenCC:
    """
    繁体字转换器
    """

    @staticmethod
    def convert(string, LANGUAGE_CODE=LANGUAGE_CODE_S2HK):
        converter = opencc.OpenCC(LANGUAGE_CODE)
        return converter.convert(string)  # 漢字


if __name__ == '__main__':
    from Comm.RobotWeb.SeleniumFramework import SeleniumFramework

    driver = SeleniumFramework()
    driver.openUrl('https://www.debug.8591.com.hk/')
    text = str(driver.getText("xpath=//div[@class=\"layout-default\"]"))
    print(text)
    driver.exitBrowser()
    textHk = MyOpenCC.convert(text)
    print(len(text) == len(textHk))
    for i in range(len(text)):
        if text[i] != textHk[i]:
            print("text:{}--textHk--{}--index:{}".format(text[i], textHk[i], i))
