import inspect
import os
import re
import time
import zipfile
import requests
from Config.BaseConfig.BaseConfig import projectPath


def Read(path, switch='read', enc='utf-8'):
    """
    读取文件数据
    path : 文件地址
    switch: 键值开关，默认格式化读取，switch=readlines一行一行读
    add_text: 添加的内容
    enc: 编码模式，默认utf-8
    return: 文件内容
    """
    f = open(path, 'r', encoding=enc)
    if switch == 'read':
        data = f.read()
        f.close()
        return data
    elif switch == 'readlines':
        data = f.readlines()
        f.close()
        return data
    else:
        return print("兄弟switch只能填readlines或者read\n")  #


# 写入文件信息函数，传入文件地址、增加的信息、开关'w'(覆盖写入)'a'(追加写入)，返回文件内容
def Write(path, add_text, switch='w'):
    """
        写入数据
    path : 文件地址
    switch: 键值开关，默认覆盖写，switch=a追加写
    add_text: 添加的内容
    enc: 编码模式
    return: 文件内容
    """
    f = open(path, switch, encoding='utf-8')
    if switch == 'a' or switch == 'w':
        data = f.write(add_text)
        f.close()
        return data
    else:
        return print("兄弟switch只能填'a'或者'w'\n")


# 修改文件信息，传入文件地址、需要修改信息的id或者name,返回文件内容
def Revise(path, id_name):
    """
    修改信息
    :param path: 文件夹地址
    :param id_name: 修改人的id或者name
    """
    while 1:
        data = Read(path, 'read')
        data = data.split("@")
        for i in range(0, len(data) - 1):
            data_change = data[i].split("|")
            # print(data_change)
            if data_change[0] == id_name or data_change[1] == id_name:
                option = input("==1修改名字==2修改年龄==3修改性别==4修改薪资==5修改密码\n")
                revise_text = input("请输入修改后的值：")
                data_change[int(option)] = revise_text
            data[i] = str(data_change[0]) + '|' + str(data_change[1]) + '|' + str(data_change[2]) + '|' + str(
                data_change[3]) + '|' + str(data_change[4]) + '|' + str(data_change[5]) + '@'
        for i in range(0, len(data)):
            if i == 0:
                f = open(path, 'w', encoding='utf-8')
                f.write(str(data[i]))
                f.close()
            else:
                f = open(path, 'a', encoding='utf-8')
                f.write(str(data[i]))
                f.close()
        return Read(path, 'read')


# 查询choose=2,3,精确查询、choose=4模糊查询
# 查询开关默认accuracy精确查找，输入vague模糊查询 返回符合的下标
def Find(pata, id_name, switch="accuracy"):
    """
    查询函数
    :param pata:文件地址
    :param id_name:查询条件id或者neme
    :param switch: 查询开关默认accuracy精确查找，输入vague模糊查询
    :return:符合要求的下标
    """
    data = Read(pata, 'readlines')
    data = data[0].split("@")
    index = []
    count = 0
    for i in range(0, len(data) - 1):
        data_info = data[i].split("|")
        if switch == 'accuracy':
            if data_info[0] == id_name or data_info[1] == id_name:
                index.append(i)
                count = 1
                break
        elif switch == 'vague':
            if data_info[0].find(id_name) != -1 or data_info[1].find(id_name) != -1:
                index.append(i)
                count += 1
        else:
            print("兄弟只能输入vague或者accuracy")
    if count == 0:
        print("查不到这个人")
    return index


# 解析payth文件，返回这样的列表[[id,name,...],[],[]...]
def Analyze_text(path):
    """
    解析payth文件，返回这样的列表[[id,name,...],[],[]...]
    :param path:文件地址
    :return:Data返回这样的列表[[id,name,...],[],[]...]
    """
    Data = []
    data = Read(path, 'readlines')
    data = data[0].split("@")
    for i in range(0, len(data) - 1):
        data_ = data[i].split("|")
        Data.append(data_)
    return Data


def Clear_environment():
    """清理测试环境"""
    os.system("taskkill /f /im chrome* >nul 2>nul")
    os.system("taskkill /f /im gecko* >nul 2>nul")
    os.system("taskkill /f /im ie* >nul 2>nul")


def CutList(List, ElementNumber):
    """大列表中几个数据组成一个小列表"""
    data = [List[i:i + ElementNumber] for i in range(0, len(List), ElementNumber)]
    return data


def TimeTranslate(Time):
    """
    可以把格式话时间转化为时间戳
    :param Time: 传入格式化的时间
    :return: 时间戳样式的时间
    """
    try:
        timeArray = time.strptime(Time, "%Y-%m-%d %H:%M:%S")
        timestamp = time.mktime(timeArray)
        return timestamp
    except:
        print('传入的时间格式不正确，需要“%Y-%m-%d %H:%M:%S”格式')
        raise  # 手动触发异常停止程序


def GetDataPath(num=0, TableName=None):
    """
    获取表格路径
    :param num:当前文件返回主目录层数
    :param TableName: 数据表格名字例如 LogisticsAdminTest/PerformanceMonitoring.xls
                      如果有父级目录则需要带上父级目录
    :return: 数据表格路径
    """
    current_path = inspect.getfile(inspect.currentframe())  # 当前.py文件路径
    DataPath = str(current_path)[::-1].split("\\", num + 1)[-1][::-1] + r'/Data'  # Data文件夹目录
    TablePath = DataPath + r'/' + TableName
    return TablePath


def getNewChromeDriver():
    """
    获取最新的谷歌驱动
    :return:
    """
    url = 'http://npm.taobao.org/mirrors/chromedriver/'
    rep = requests.get(url).text

    time_list = []  # 用来存放版本时间
    time_version_dict = {}  # 用来存放版本与时间对应关系

    result = re.compile(r'\d.*?/</a>.*?Z').findall(rep)  # 匹配文件夹（版本号）和时间

    for i in result:
        Time = i[-24:-1]  # 提取时间
        version = re.compile(r'.*?/').findall(i)[0]  # 提取版本号
        time_version_dict[Time] = version  # 构建时间和版本号的对应关系，形成字典
        time_list.append(Time)  # 形成时间列表
    # 删除最大的
    time_list.remove(max(time_list))
    latest_version = time_version_dict[max(time_list)]  # 用最大（新）时间去字典中获取最新的版本号
    download_url = url + latest_version + 'chromedriver_win32.zip'  # 拼接下载链接
    file = requests.get(download_url)
    chromeDriverPath = projectPath + '/ChromeDriver'
    with open(chromeDriverPath + "/chromedriver.zip", 'wb') as zip_file:  # 保存文件到脚本所在目录
        zip_file.write(file.content)
        zip_file.close()
    # 解压到ChromeDriver到指定目录
    unzip(chromeDriverPath + "/chromedriver.zip", chromeDriverPath)


def unzip(path, folder_abs):
    """
    基本格式：zipfile.ZipFile(filename[,mode[,compression[,allowZip64]]])
    mode：可选 r,w,a 代表不同的打开文件的方式；r 只读；w 重写；a 添加
    compression：指出这个 zipfile 用什么压缩方法，默认是 ZIP_STORED，另一种选择是 ZIP_DEFLATED；
    allowZip64：bool型变量，当设置为True时可以创建大于 2G 的 zip 文件，默认值 True；
    """
    zip_file = zipfile.ZipFile(path)
    zip_list = zip_file.namelist()  # 得到压缩包里所有文件

    for f in zip_list:
        zip_file.extract(f, folder_abs)  # 循环解压文件到指定目录

    zip_file.close()  # 关闭文件，必须有，释放内存

