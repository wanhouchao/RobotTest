# -*- coding: utf-8 -*-

"""
封装log方法

"""
import datetime
import logging
import os
import time

from Config.BaseConfig.BaseConfig import projectPath

LEVELS = {
    'debug': logging.DEBUG,  # 10
    'info': logging.INFO,  # 20
    'warning': logging.WARNING,  # 30
    'error': logging.ERROR,  # 40
    'critical': logging.CRITICAL  # 50
}

logger = logging.getLogger()
level = 'default'


def create_file(filename):
    """创建文件夹"""
    path = filename[:filename.replace('\\', '/').rfind('/')]
    if not os.path.isdir(path):
        os.makedirs(path)
    if not os.path.isfile(filename):
        fd = open(filename, mode='w', encoding='utf-8')
        fd.close()


def set_handler(levels):
    if levels == 'error':
        logger.addHandler(MyLog.err_handler)
    logger.addHandler(MyLog.handler)


def remove_handler(levels):
    if levels == 'error':
        logger.removeHandler(MyLog.err_handler)
    logger.removeHandler(MyLog.handler)


def get_current_time():
    return time.strftime(MyLog.date, time.localtime(time.time()))


def clearLog():
    today = datetime.date.today()
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    logList = os.listdir(projectPath + "/Log")
    for log in logList:
        if log.find(str(today)) != -1 or log.find(str(yesterday)) != -1:
            break
        else:
            os.remove(projectPath + "/Log/" + log)


class MyLog:
    # 清理日志只保留两天的
    clearLog()
    # 获取当前主文件夹地址
    path = projectPath
    # log日志的地址 日志文件名%Y-%m-%d-log.log
    log_file = path + '/Log/' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + '-log.log'
    create_file(log_file)
    err_file = path + '/Log/' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + '-err.log'
    create_file(err_file)

    logger.setLevel(LEVELS.get(level, logging.NOTSET))  # 定义log等级 只输出这个等级之上的
    # 时间格式
    date = '%Y-%m-%d %H:%M:%S'

    # 将日志信息输出到指定文件中
    handler = logging.FileHandler(log_file, encoding='utf-8')
    err_handler = logging.FileHandler(err_file, encoding='utf-8')

    @staticmethod
    def debug(log_meg):
        set_handler('debug')
        logger.debug("[DEBUG " + get_current_time() + "]" + str(log_meg))
        remove_handler('debug')

    @staticmethod
    def info(log_meg):
        set_handler('info')
        logger.info("[INFO " + get_current_time() + "]" + str(log_meg))
        remove_handler('info')

    @staticmethod
    def warning(log_meg):
        set_handler('warning')
        logger.warning("[WARNING " + get_current_time() + "]" + str(log_meg))
        remove_handler('warning')

    @staticmethod
    def error(log_meg):
        set_handler('error')
        logger.error("[ERROR " + get_current_time() + "]" + str(log_meg))
        remove_handler('error')

    @staticmethod
    def critical(log_meg):
        set_handler('critical')
        logger.error("[CRITICAL " + get_current_time() + "]" + str(log_meg))
        remove_handler('critical')


if __name__ == '__main__':
    MyLog.debug("123132132")
