import inspect
import xlrd

from Config.BaseConfig.BaseConfig import projectPath


class ReadExcel:
    def __init__(self, path):
        """传excel地址path"""
        try:
            self.data = xlrd.open_workbook(path)  # data--excel数据
        except Exception as e:
            print("请传入正确的地址！！！", e)

    def GetSheetsName(self):
        """获取sheet名字列表"""
        table_list = self.data.sheet_names()
        return table_list

    def GetTable(self, string_ele):
        """
        传入下标索引index或者表格名字name返回表格数据
        示例：string_ele => name=tableName
        """
        if str(string_ele).find('=') == -1:
            raise print('元素的方式错误，无法解析')  # raise手动触发异常
        else:
            method = str(string_ele).split('=')[0]
            ele = str(string_ele).split('=')[1]
            if method == 'index':
                try:
                    table = self.data.sheet_by_index(int(ele))
                    return table
                except Exception as e:
                    print('没有找到这个索引的表格！！！', e)
            elif method == 'name':
                try:
                    table = self.data.sheet_by_name(ele)
                    return table
                except Exception as e:
                    print('没有找到这个名字的表格！！！', e)
            else:
                print('格式错误,只能按照index=xxx,name=xxx的格式写入！！！')

    def ReadRowsTable(self, string_ele):
        """
        按照表index和name一行一行读取返回读取列表Value_list
        示例：string_ele => name=tableName
        """
        valueList = []
        table = self.GetTable(string_ele)
        # print(table)
        for i in range(0, table.nrows):
            valueList.append(table.row_values(i))
        return valueList

    def getData(self, string_ele):
        """
        处理数据
        示例：string_ele => name=tableName
        """
        data_list = self.ReadRowsTable(string_ele)
        DataTable = {'title': data_list[0]}
        for i in range(1, len(data_list)):
            DataTable['test_case%s' % i] = data_list[i]
        return DataTable  # 生成一个字典储存数据 title为输入字段
        # 例如：{'test_case1': [1.0, 2.0, 3.0, 4.0, 5.0],
        # 'title': ['所属机构*', '充电位置*', '机器人*', '检测距离配置(米)', '备注']}


def GetDataPath(TableName=None, dirName='EpsitRobotTest'):
    """
    获取表格路径
    :param dirName: 主文件夹名字
    :param TableName: 数据表格名字例如 LogisticsAdminTest/PerformanceMonitoring.xls
                      如果有父级目录则需要带上父级目录
    :return: 数据表格路径
    """
    current_path = inspect.getfile(inspect.currentframe()).replace('\\', '/')  # 当前.py文件路径 使用replace把\转化为/
    PathList = str(current_path).split("/")  # 获取文件列表
    for i in range(len(PathList)):
        if PathList[i] == dirName:
            dirPath = str(current_path)[::-1]  # eps文件夹目录
            dataPath = dirPath.split('/', i)[-1][::-1]
            if str(TableName)[0] == '/':
                TablePath = dataPath + TableName
                # print("1---",TablePath)
                return TablePath
            elif str(TableName)[0] != '/' and TableName is not None:
                TablePath = dataPath + '/' + TableName
                # print(TablePath)
                return TablePath


def GetData(string_ele, TableName):
    """
    示例：string_ele => name=tableName
    TableName=None（数据到文件夹的目录）, dirName='EpsitRobotTest'(文件夹名字)
    """
    # 通过Config里面获取项目目录拼接出需要的数据目录
    DataPath = projectPath + "/Data/LogisticsAdminTest/%s/%s.xlsx" % (TableName, TableName)
    # print("DataPath:",DataPath)
    Excel = ReadExcel(DataPath)
    data = Excel.getData(string_ele)  # 传表格name或者index访问表格数据
    Data = {}  # 格式化数据 方便测试使用ddt
    title = data.pop('title')
    # 把字段前面加的*去掉
    for i in range(len(title)):
        if str(title[i]).find("*") != -1:
            title[i] = title[i][:-1]
    for test_case in data:
        List = {}
        for i in range(len(data[test_case])):
            List[title[i]] = data[test_case][i]
        Data[test_case] = List
    # 格式化后数据数据 在数据前加上字段名称方便调用
    # {'test_case1': {'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
    return Data  # 字典类型的数据


if __name__ == '__main__':
    testCaseData = GetData(string_ele="name=机器人部件管理", TableName='机器人管理')  # 传表格name或者index访问表格数据
    print(testCaseData)
