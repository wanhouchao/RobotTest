import unittest

from selenium.common.exceptions import TimeoutException


class Assert(unittest.TestCase):
    """
    封装断言
    """

    # 接口返回信息断言 一般是json格式，校验包括字段类型，必传字段
    @staticmethod
    def assertApiResponse(response: dict, expectedResult: dict):
        """
        接口返回信息断言 一般是json格式，校验包括字段类型，必传字段
        :param response: 接口返回的实际结果
        :param expectedResult: 预期接口返回的结果
        :return: 校验结果为假直接触发异常，返回结果
        """
        lackFieldError = []  # 缺少的字段
        fieldTypeError = []  # 字段类型错误
        valueError = []  # value错误
        # 遍历预期结果和response对比
        for key, value in expectedResult.items():
            # 判断必穿字段是否传
            if key not in response:
                lackFieldError.append("返回结果缺少字段:{}".format(key))
            elif type(response.get(key)) != type(value):
                fieldTypeError.append(("字段{}值数据类型不对,预期:{},返回:{}".format(key, type(value), type(response[key]))))
            elif response.get(key) != value:
                valueError.append(("字段{}值数据不对,预期:{},返回:{}".format(key, value, response.get(key))))
        if len(lackFieldError) == 0 and len(fieldTypeError) == 0 and len(valueError) == 0:
            return True
        else:
            raise AssertionError("\n缺少字段错误：\n\t{}\n字段类型错误：\n\t{}\n返回value错误：\n\t{}".format(
                "\n\t".join(lackFieldError),
                "\n\t".join(fieldTypeError),
                "\n\t".join(valueError)
            ))

    @staticmethod
    def assertUiElementExists(browser, string_ele):
        """
        通过selenium判断元素是否存在需要传浏览器对象和元素定位方式
        :param browser:浏览器对象必须是SeleniumFramework创建的浏览器对象
        :param string_ele:元素定位详情看SeleniumFramework里面 例如：xpath=//*[@id=\"su\"]
        :return:
        """
        if len(browser.findElements(string_ele)) == 0:
            raise AssertionError("当前页面无法找到该元素：{}".format(string_ele))
        return True

    @staticmethod
    def assertUiElementText(browser, string_ele, expectedResult):
        """
        通过提取的文本和预期对比
        :param browser: 浏览器对象必须是SeleniumFramework创建的浏览器对象
        :param string_ele: 元素定位详情看SeleniumFramework里面 例如：xpath=//*[@id=\"su\"]
        :param expectedResult: 预期文本
        :return:
        """
        # 添加断言
        try:
            actualResult = browser.getText(string_ele)
        except TimeoutException:
            raise AssertionError("当前页面无法找到该元素：{}".format(string_ele))

        if actualResult != expectedResult:
            raise AssertionError("预期结果和实际结果不符,预期结果：{},实际结果：{}".format(expectedResult, actualResult))
        return True


if __name__ == '__main__':
    unittest.main()
