import time
import unittest
from Comm.BaseFrame.Log import MyLog
from Comm.RobotApi.BaseApi import BaseApi
from Comm.RobotWeb.SeleniumFramework import SeleniumFramework
from Config.ApiConfig.ApiConfig import *
from Config.BaseConfig.BaseConfig import projectPath


class unittestApi(unittest.TestCase):
    """
       这个里面是测试用例添加的公共方法，分离出来方便维护
       这个是api的公共方法
       分类：web就是站台前端的业务流程 admin就是后台的业务流程
    """

    def webGetToken(self, url=LOGIN_URL, jsonData=apiUserData, headers=apiHeaders):
        """
        后端检验规则就是要传一个 Authorization:Bearer token做校验
        :param url:
        :param jsonData:
        :param headers:
        :return:请求头里面添加Authorization字段校验token
        """
        # 请求登录接口获取token
        self.api = BaseApi()
        response = self.api.POST(url=url, json=jsonData, headers=headers).json()
        responseMsg = response["message"]
        responseData = response["data"]
        try:
            apiHeaders["Authorization"] = 'Bearer ' + responseData.get("access_token")
            MyLog.info("获取token成功-->{}，message-->{}".format(apiHeaders["Authorization"], responseMsg))
            return response
        except TypeError as TE:
            MyLog.error("获取access_token失败-->{}，data-->{},错误码-->{}".format(responseMsg, responseData, TE))

    def webGetAllInfo(self):
        self.webGetToken()
        response = self.api.GET("https://www.debug.8591.com.hk/node/user/allInfo", headers=apiHeaders)
        print("apiHeaders-->", apiHeaders)
        print("response-->", response.json())

    @staticmethod
    def webUserRegister(url=REGISTER_URL, data=registerData):
        """
        用户注册
        :return:response
        """
        response = BaseApi.POST(url=url, json=data, headers=apiHeaders).json()
        MyLog.info("注册账号：{}，注册结果：{}".format(data["mobile"], response))
        # 记录生成的账号
        with open(projectPath + "/Data/user.txt", "a+") as f:
            f.write("注册账号：{}，密码：{}，用户id：{}\n".format(data["mobile"], data["password"], response["data"]["user_id"]))
            f.close()
        print("注册账号：{}，密码：{}，用户id：{}\n".format(data["mobile"], data["password"], response["data"]["user_id"]))
        return response

    def webApplyAuth(self, mobile, password="123456a"):
        """
        实名认证
        :return:response
        """
        jsonData = {
            "mobile": mobile,
            "password": password
        }
        self.webGetToken(jsonData=jsonData)
        url = "https://www.debug.8591.com.hk/api/v3/member/apply-auth"
        data = {
            'cert_number': "C{}(E)".format(str(random.randint(100000, 999999))),  # 身份证号
            'cert_pic_back': "images/card/20200817/obQveicbvwAO8fXFtbE68SEWDxesCVyGBpkmLje0.png",  # 图片随便传
            'cert_pic_front': "images/card/20200817/vWVnqUs3Sj1HUyu99Ylrgd96h1F8HyL6JMc7dnTw.png",  # 图片随便传
            'cert_type': "1",  # 身份证类型
            'english_name': "wanhc",  # 英文名
            'issue_region': "hk",  # 香港还是台湾
            'name': "万厚超",  # 姓名
            'sex': "male"  # 性别
        }
        response = BaseApi.POST(url=url, json=data, headers=apiHeaders).json()
        MyLog.info("实名账号：{}，实名结果：{}".format(jsonData["mobile"], response))
        print("实名账号：{}，实名结果：{}".format(jsonData["mobile"], response))
        return response

    @staticmethod
    def adminAuthAudit(userId, state=1):
        """
        后台实名审核
        :param state:操作 1是通过 -1拒绝 -2跟踪默认是1
        :param userId:用户id
        :return:response
        """
        # 获取id和state
        br = SeleniumFramework(browserPattern=False)
        # 登录
        br.openUrl('https://admin.debug.8591.com.hk/')
        br.doWrite("id=input-staff-id", "10810")
        br.doWrite("id=input-password", "123456")
        br.onClick("xpath=//button[text()=\" 登入後台 \"]")
        time.sleep(3)
        br.onClick("xpath=//a[text()=\"實名認證\"]")
        # 操作
        if state == 1:
            br.onClick("xpath=//a[text()=\"{}\"]/../../../td[12]/div/a[@title=\"通過\"]".format(userId))
            br.onClick("xpath=//a[text()=\"確定\"]")
            MyLog.info("通過成功！！！")
            print("实名审核通過成功！！！")
        elif state == -1:
            br.onClick("xpath=//a[text()=\"{}\"]/../../../td[12]/div/a[@title=\"拒绝\"]".format(userId))
            br.switchToFrame("layui-layer-iframe2")
            br.onClick("xpath=//a[text()=\"確認拒絕\"]")
            MyLog.info("拒绝成功！！！")
            print("实名审核拒绝成功！！！")
        elif state == -2:
            br.onClick("xpath=//a[text()=\"{}\"]/../../../td[12]/div/a[@title=\"跟踪\"]".format(userId))
            br.onClick("xpath=//a[text()=\"確定\"]")
            MyLog.info("跟踪成功！！！")
            print("实名审核跟踪成功！！！")
        else:
            print("没有这个操作！！！")
            raise MyLog.error("没有这个操作！！！")
        br.exitBrowser()
