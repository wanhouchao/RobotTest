import requests
import json
from Config.ApiConfig.ApiConfig import *
from Comm.BaseFrame.Log import MyLog


class BaseApi:

    @staticmethod
    def GET(url, **kwargs):
        return requests.get(url=url, **kwargs)

    @staticmethod
    def POST(url, headers=apiHeaders, **kwargs):
        return requests.post(url=url, headers=headers, **kwargs)

    @staticmethod
    def DELETE(url, headers=apiHeaders, **kwargs):
        return requests.delete(url=url, headers=headers, **kwargs)

    @staticmethod
    def PUT(url, headers=apiHeaders, **kwargs):
        return requests.put(url=url, headers=headers, **kwargs)

    def sendHttpRequest(self, url, method="get", **kwargs):
        if method.lower() == 'get':
            return self.GET(url, **kwargs)
        elif method.lower() == "post":
            return self.POST(url, **kwargs)
        elif method.lower() == "delete":
            return self.DELETE(url, **kwargs)
        elif method.lower() == "put":
            return self.PUT(url, **kwargs)
        else:
            MyLog.error("请求传入方法错误")
            raise


if __name__ == '__main__':
    def addCart():
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'PostmanRuntime/7.26.2',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
        }
        url_ = ''
        data = {
            "mobile": "85918599",
            "password": "123123a"
        }
        session = requests.session()
        access_token = session.post(url_, json=data, headers=headers).json()["data"].get("access_token")
        # 获取
        headers["Authorization"] = 'Bearer ' + access_token
        # 添加购物车
        addCartUrl = 'https://www.debug.8591.com.hk/api/v3/cart/update-cart'
        addData = {'goods_id': "3980598", 'type': "goods", 'quantity': 666666, 'id': 28921}
        print(session.post(addCartUrl, json=addData, headers=headers).json())


    params = {
        'game_id': 111,
        'server_id': '',
        'goods_type_id': 1,
        'sort_by': '',
        'sort_val': '',
        'handover_at': '',
        'start_price': '',
        'end_price': '',
        'is_complete': '',
        'keyword': '',
        'page': 1,
        'limit': 20,
        'dev': 0,
    }
    print(requests.get(
        url='https://www.debug.8591.com.hk/node/mall/list', params=params,
        headers=apiHeaders).json())
