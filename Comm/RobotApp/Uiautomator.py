import os
import uiautomator2 as u2
from Comm.BaseFrame.Log import MyLog
from Config.AppConfig.AppConfig import devicesName, packageName, Y1, Y2, E2


class UiautomatorFrame:
    def __init__(self, connectWay='usb', deviceName=devicesName):
        # 连接手机 默认是USB连接
        if connectWay == 'usb':
            self.driver = u2.connect_usb(deviceName)
            MyLog.debug("usb连接设备成功")
        elif connectWay == 'wifi':
            self.driver = u2.connect_wifi(deviceName)
            MyLog.debug("wifi连接设备成功")
        else:
            self.driver = u2.connect(deviceName)
            MyLog.debug("连接设备成功")

    def openApp(self, appPackageName=packageName):
        """
        打开对应app
        :param appPackageName:app包名默认是那的配置文件读到的
        :return: None
        """
        self.driver.app_start(appPackageName)

    def __Write_element(self, string, time=10):
        """增加框架稳定性  每次操作元素之前都先智能等待一下元素在页面上是否可见"""
        if str(string).find('=') == -1:
            raise print('元素输入错误，找不到元素')  # raise 手动出发异常
        else:
            Way = str(string).split('=', 1)[0]
            Str = str(string).split('=', 1)[1]
            if Way == "text":  # 文本定位元素
                self.driver(text=Str).exists(timeout=time)
            elif Way == "resourceId":  # id定位元素
                Str = packageName + ':id/' + Str  # 自动拼接id
                self.driver(resourceId=Str).exists(timeout=time)
            elif Way == "xpath":  # 路径定位元素
                if Str.find('@resource-id='):
                    # //*[@resource-id="com.eps.logistics.ytwo:id/rl_home_transport"]
                    if Str.find(Y1.get("packageName")):
                        Str = Str.replace(Y1.get("packageName"), packageName)
                    elif Str.find(Y2.get("packageName")):
                        Str = Str.replace(Y2.get("packageName"), packageName)
                    elif Str.find(E2.get("packageName")):
                        Str = Str.replace(E2.get("packageName"), packageName)
                self.driver.xpath(Str).wait(timeout=time)
            elif Way == "description":  # 描述定位元素
                self.driver(description=Str).exists(timeout=time)
            else:
                MyLog.error('没有找到这个元素或者无法解析！')
                raise print('没有找到这个元素或者无法解析！')

    def findElement(self, string, time=10):
        """
        分析传入字符串传到对应的方式里面去,找元素
        :param string: 定位方式 例如：text resourceId xpath description
        传入形式如：id=user
        :param time: 等待超时时间
        :return:
        """
        self.__Write_element(string, time=time)  # 等待元素再页面可见再做下一步动作
        if str(string).find('=') == -1:
            raise print('元素输入错误，找不到元素')  # raise 手动出发异常
        else:
            Way = str(string).split('=', 1)[0]
            Str = str(string).split('=', 1)[1]
            if Way == "text":  # 文本定位元素
                return self.driver(text=Str)
            elif Way == "resourceId":  # id定位元素
                Str = packageName + ':id/' + Str
                return self.driver(resourceId=Str)
            elif Way == "xpath":  # 路径定位元素 现在官方不支持 xpath定位
                if Str.find('@resource-id='):
                    if Str.find(Y1.get("packageName")):
                        Str = Str.replace(Y1.get("packageName"), packageName)
                    elif Str.find(Y2.get("packageName")):
                        Str = Str.replace(Y2.get("packageName"), packageName)
                    elif Str.find(E2.get("packageName")):
                        Str = Str.replace(E2.get("packageName"), packageName)
                return self.driver.xpath(Str)
            elif Way == "description":  # 描述定位元素
                return self.driver(description=Str)
            else:
                raise print('其他方法还没有开发，敬请期待！！！')

    def onClick(self, string, time=5):
        # 按照控件点击元素
        ele = self.findElement(string, time=time)
        try:
            ele.click()
        except Exception as e:
            MyLog.error("--%s--元素无法点击：%s" % (string.split('=', 1)[1], e))
            # raise print("--%s--元素无法点击" % string.split('=', 1)[1])

    def doubleClick(self, x, y, time=0.1):
        """
        坐标点双击
        :param x:x坐标
        :param y:y坐标
        :param time:按压时间
        :return:None
        """
        self.driver.double_click(x, y, duration=time)

    def doWrite(self, string, text, time=5):
        """
        :param string:元素
        :param time:等待最长时间
        :param text:输入文本
        """
        self.driver.set_fastinput_ime(True)  # 切换输入法
        ele = self.findElement(string, time=time)
        try:
            ele.clear_text()
            ele.send_keys(text)
        except Exception as e:
            MyLog.error("--%s--元素无法写入:%s" % (string.split('=')[1], e))
            # raise print("--%s--元素无法写入" % string.split('=')[1])

    def clickCoordinate(self, x, y, duration=None):
        """
        点击坐标(x,y)
        默认短按
            long click at arbitrary coordinates.
            duration (float): seconds of pressed
        """
        if str(duration).isalnum():
            try:
                self.driver().long_click(x, y, duration=int(duration))
            except Exception:
                MyLog.error("(%s,%s)--元素无法点击--长按" % (x, y))
                raise print("(%s,%s)--元素无法点击--长按" % (x, y))
        else:
            try:
                self.driver().click(x, y)
            except Exception:
                MyLog.error("(%s,%s)--元素无法点击--短按" % (x, y))
                raise print("(%s,%s)--元素无法点击--短按" % (x, y))

    def screenshot(self, path):
        """
        截图方法 传入储存图片的地址
        :param path: 本地储存图片的地址
        """
        filePath = path.replace('\\', '/')[1][::-1]  # 图片父级地址
        if not os.path.exists(filePath):
            os.mkdir(filePath)
        self.driver.screenshot(path)

    def getElementInfo(self, string, time=10):
        """
        提取元素属性值
        :return: 返回元素所有信息 json格式
        """
        ele = self.findElement(string, time=time)
        return ele.info  # 提取元素里面所有属性值 json格式

    def exitApp(self, appPackageName=packageName):
        """
        退出App
        """
        self.driver.app_stop(appPackageName)

    def getScreenshot(self, path):
        """截图操作"""
        self.driver.screenshot(path)

    def elePoll(self, string):
        """轮询页面元素是否存在"""
        pass

    def getToast(self):
        """获取toast信息，如果没有抓到就打印None"""
        return self.driver.toast.get_message()

    def aaa(self):
        """获取整个页面的元素"""
        print(self.driver.dump_hierarchy())
        return self.driver.dump_hierarchy()


if __name__ == '__main__':
    devices = UiautomatorFrame(deviceName="127.0.0.1:21503")
    devices.openApp(appPackageName="com.addcn.hk8591app")
    # print(devices.findElement(
    #     "xpath=//*[@resource-id=\"android:id/content\"]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.widget.ImageView[5]").info)
    devices.aaa()
    # print(devices.findElement("text=立即登入").info)
