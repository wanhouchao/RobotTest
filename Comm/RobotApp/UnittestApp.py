import time
import unittest
from Comm.BaseFrame.Log import MyLog
from Comm.RobotApp.Uiautomator import UiautomatorFrame
from Config.AppConfig.AppConfig import packageName, appUser, appPassword
from Config.BaseConfig.BaseConfig import projectPath


class unittestApp(unittest.TestCase):
    """
       这个里面是测试用例添加的公共方法，分离出来方便维护
       这个是app端的公共方法
    """

    def loginApp(self):
        """app登录"""
        self.appDriver = UiautomatorFrame()  # 定义一个app驱动
        self.appDriver.openApp(packageName)
        print(self.appDriver.findElement('text=登录', time=2).exists())
        if self.appDriver.findElement('text=登录', time=2).exists():
            self.appDriver.driver.set_fastinput_ime(True)  # 切换输入法
            # 先切换登录为模拟地盘
            self.appDriver.doWrite('resourceId=et_login_account', 123)
            self.appDriver.doWrite('resourceId=et_login_password', 321)
            self.appDriver.onClick('text=登录')
            self.appDriver.onClick('text=模拟底盘')
            # 判断是否打勾
            if self.appDriver.findElement('text=是否开启模拟底盘').info.get('checked') is False:
                self.appDriver.onClick('text=是否开启模拟底盘')
            if packageName != "com.example.administrator.websc":
                self.appDriver.doWrite('resourceId=et_address', '192.168.3.33')
            self.appDriver.doWrite('resourceId=et_account', appUser)
            self.appDriver.onClick('text=确认')
            try:
                self.appDriver.onClick('text=退出程序', time=2)  # 云际app解析字符串会异常暂时这样处理
            except Exception as e:
                print(e)
            # 重新登录
            self.appDriver.openApp(packageName)
            self.appDriver.doWrite('resourceId=et_login_account', appUser)
            self.appDriver.doWrite('resourceId=et_login_password', appPassword)
            self.appDriver.onClick('text=登录')
        # 判断是否启动app失败 走重启逻辑
        if self.appDriver.findElement("resourceId=btn_start_retry", time=3).exists() is True:
            for i in range(3):
                if self.appDriver.findElement("resourceId=btn_start_retry", time=1).exists():
                    time.sleep(15)
                    try:
                        print("点击重试%s次" % (i + 1))
                        self.appDriver.onClick("resourceId=btn_start_retry", time=2)
                    except Exception as e:
                        print('没有找到重试按钮', e)
                        break
        if self.appDriver.findElement("resourceId=pb_home_battery", time=15).exists() is False:
            self.appDriver.getScreenshot(
                projectPath + "/Config/Image/启动app失败-{}.jpg".format(
                    time.strftime('%Y-%m-%d %H-%M-%S', time.localtime(time.time()))))
            MyLog.error("启动app失败")
            raise print('启动app失败')
        MyLog.debug("启动app成功")
        return self.appDriver

    def inputPassword(self):
        """验证密码"""
        self.appDriver.onClick('resourceId=btn_pwd_btn21')
        self.appDriver.onClick('resourceId=btn_pwd_btn22')
        self.appDriver.onClick('resourceId=btn_pwd_btn23')
        self.appDriver.onClick('resourceId=btn_pwd_btn24')

    def robotStop(self):
        """配送软急停"""
        self.appDriver.onClick('resourceId=iv_status_show_carrier')
        self.appDriver.onClick('resourceId=iv_status_show_carrier')

    def escTask(self):
        """取消任务"""
        self.robotStop()
        self.appDriver.onClick('resourceId=btn_status_tasks_cancel')
        self.appDriver.onClick('text=测试')  # 选择用户
        self.inputPassword()

    def Test(self, string):
        print(self.appDriver.findElement(string).info)

    # 设置界面操作
    def goSet(self):
        """首页进入设置界面"""
        self.appDriver.onClick("resourceId=iv_home_other")
        self.appDriver.onClick("resourceId=tv_home_other_set")
        if self.appDriver.findElement("resourceId=rv_pwd_user", time=2).exists:
            # 判断有没有用户验证
            self.appDriver.onClick("text=测试")
            self.inputPassword()

    def idVerify(self, Set=True, disinfect=False, transport=True, cancelTask=True, accept=True):
        """进入设置界面后，身份验证,身份验证默认全部打开"""
        # 将进入到访问权限界面
        self.appDriver.onClick("text=访问权限")
        time.sleep(1)
        # 读取按钮状态
        setStatus = self.appDriver.getElementInfo("resourceId=sw_setting_permission_set", time=2)["checked"]
        disinfectStatus = self.appDriver.getElementInfo("resourceId=sw_setting_permission_disinfect", time=2)["checked"]
        transportStatus = self.appDriver.getElementInfo("resourceId=sw_setting_permission_transport", time=2)["checked"]
        cancelTaskStatus = self.appDriver.getElementInfo("resourceId=sw_setting_permission_canceltask", time=2)[
            "checked"]
        acceptStatus = self.appDriver.getElementInfo("resourceId=sw_setting_permission_accept", time=2)["checked"]
        # 状态不一致则点击一下，默认全部打开
        if Set != setStatus:
            self.appDriver.onClick("resourceId=sw_setting_permission_set", time=2)
        if disinfect != disinfectStatus:
            self.appDriver.onClick("resourceId=sw_setting_permission_disinfect", time=2)
        if transport != transportStatus:
            self.appDriver.onClick("resourceId=sw_setting_permission_transport", time=2)
        if cancelTask != cancelTaskStatus:
            self.appDriver.onClick("resourceId=sw_setting_permission_canceltask", time=2)
        if accept != acceptStatus:
            self.appDriver.onClick("resourceId=sw_setting_permission_accept", time=2)

    def functionSwitch(self, **kwargs):
        """
        访问权限界面的开关
        "door", "acoustoOptic", "aotuReceive",
        "medicine", "blueposition","pwd","userlist"关键字
        set, disinfect, transport, cancelTask, accept
        :param kwargs: 传入开关按钮的状态
        """
        # 将进入到访问权限界面
        self.appDriver.onClick("text=访问权限")
        time.sleep(1)

        def switchClick(String, Value):
            """点击开关"""
            ele = self.appDriver.findElement(String, time=2)
            status = ele.info["checked"]
            if status != Value:
                ele.click()

        for key, value in kwargs.items():
            # 流程简化开关
            if key == "door":  # 自动开门
                string = "resourceId=sw_setting_permission_door"
                switchClick(string, value)
            elif key == "acoustoOptic":  # 声光提醒
                string = "resourceId=sw_setting_permission_acousto_optic"
                switchClick(string, value)
            elif key == "aotuReceive":  # 自动接收
                string = "resourceId=sw_setting_permission_aotuReceive"
                switchClick(string, value)
            elif key == "medicine":  # 退药
                string = "resourceId=sw_setting_return_medicine"
                switchClick(string, value)
            elif key == "blueposition":  # 蓝牙定位楼层
                string = "resourceId=sw_setting_permission_blueposition"
                switchClick(string, value)
            elif key == "pwd":  # 密码输入不显示
                string = "resourceId=sw_show_pwd"
                switchClick(string, value)
            elif key == "userlist":  # 用户列表排序(正序)
                string = "resourceId=sw_user_list_sort"
                switchClick(string, value)
            # 身份验证开关
            elif key == "set":  # 设置
                string = "resourceId=sw_setting_permission_set"
                switchClick(string, value)
            elif key == "disinfect":  # 消毒
                string = "resourceId=sw_setting_permission_disinfect"
                switchClick(string, value)
            elif key == "transport":  # 配送
                string = "resourceId=sw_setting_permission_transport"
                switchClick(string, value)
            elif key == "cancelTask":  # 取消任务
                string = "resourceId=sw_setting_permission_canceltask"
                switchClick(string, value)
            elif key == "accept":  # 接收
                string = "resourceId=sw_setting_permission_accept"
                switchClick(string, value)

    def waitTime(self, waitTobe=None, accept=None, appStart=None, intoSecond=30, outSecond=30, outTime2pre=60):
        """等待时间"""
        self.appDriver.onClick("text=等待时间")
        time.sleep(1)

        def setTime(string, value):
            ele = self.appDriver.findElement(string, time=2)
            if value is not None:
                if str(value).find(":") != -1:
                    minute = str(value).split(":")[0]
                    second = str(value).split(":")[1]
                    if ele.info["checked"] is False:
                        ele.click()
                    self.appDriver.doWrite("resourceId=et_" + string.split("_", 1)[1] + "_minute", minute)
                    self.appDriver.doWrite("resourceId=et_" + string.split("_", 1)[1] + "_second", second)
                else:
                    self.appDriver.doWrite("resourceId=et_" + string.split("_", 1)[1] + "_minute", value)
            else:
                if ele.info["checked"] is True:
                    ele.click()

        # 设置等待使时间
        setTime("resourceId=sw_setting_wait_tobe", value=waitTobe)
        setTime("resourceId=sw_setting_wait_accept", value=accept)
        setTime("resourceId=sw_setting_app_start", value=appStart)
        self.appDriver.doWrite("resourceId=et_setting_into_second", intoSecond)
        self.appDriver.doWrite("resourceId=et_setting_out_second", outSecond)
        self.appDriver.doWrite("resourceId=et_outTime2pre", outTime2pre)
        self.appDriver.onClick("resourceId=btn_setting_wait_confirm")
        msg = self.appDriver.getToast()
        print(msg)
