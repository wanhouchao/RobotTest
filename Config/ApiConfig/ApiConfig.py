# 放置api常用的数据配置
import random

apiHeaders = {  # 请求头参数
    'Content-Type': 'application/json',
    'User-Agent': 'PostmanRuntime/7.26.2',
    'Accept': 'application/json, text/plain, */*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
}
# 用户名密码
apiUser = "1084199"
apiPassword = "123123a"
apiUserData = {
    "mobile": apiUser,
    "password": apiPassword
}
# 用户注册默认数据
registerData = {
    "code": "123456",  # 验证码，必填，测试环境随便填跳过验证
    "email": "",
    "mobile": "8" + str(random.randint(1000000, 9999999)),  # 手机号,必填唯一
    "name": "万厚超",  # 用户名，必填可以重复
    "password": "123456a",  # 密码统一默认是123456a，必填
    "reg_client": "web",  # 注册端
}
# url管理
BASE_URL = "https://www.debug.8591.com.hk/api/v3"
LOGIN_URL = BASE_URL + "/user/login"  # 登录接口
REGISTER_URL = BASE_URL + "/user/register"  # 注册接口
