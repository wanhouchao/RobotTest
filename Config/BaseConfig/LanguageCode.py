# 语言转换标签
LANGUAGE_CODE_S2T = "s2t.json"  # 简体中文到繁体中文简体到繁体
LANGUAGE_CODE_T2S = "t2s.json"  # 繁体中文到简体中文繁体到简体
LANGUAGE_CODE_S2TW = "s2tw.json"  # 简体中文到繁体中文（台湾标准）简体到台湾正体
LANGUAGE_CODE_TW2S = "tw2s.json"  # 繁体中文（台湾标准）到简体中文台湾正体到简体
LANGUAGE_CODE_S2HK = "s2hk.json"  # 简体中文到繁体中文（香港变体）简体到香港繁体
LANGUAGE_CODE_HK2S = "hk2s.json"  # 繁体中文（香港变体）到简体中文香港繁体到简体
LANGUAGE_CODE_S2TWP = "s2twp.json"  # 带有台湾成语简体到繁体（台湾正体标准）并转换为台湾常用词汇的简体中文到繁体中文（台湾标准）
LANGUAGE_CODE_TW2SP = "tw2sp.json"  # 繁体中文（台湾标准）到简体中文与大陆成语繁体（台湾正体标准）到简体并转换为中国大陆常用词汇
LANGUAGE_CODE_T2TW = "t2tw.json"  # 繁体中文（OpenCC标准）到台湾标准繁体（OpenCC标准）到台湾正体
LANGUAGE_CODE_HK2T = "hk2t.json"  # 繁体中文（香港变体）到繁体中文香港繁体到繁体（OpenCC标准）
LANGUAGE_CODE_T2HK = "t2hk.json"  # 繁体中文（OpenCC标准）至香港变体繁体（OpenCC标准）到香港繁体
LANGUAGE_CODE_T2JP = "t2jp.json"  # 繁体字（Kyūjitai）到新日文汉字（Shinjitai）繁体（OpenCC标准，旧字体）到日文新字体
LANGUAGE_CODE_JP2T = "jp2t.json"  # 新日文汉字（Shinjitai）转换为繁体字（Kyūjitai）日文新字体到繁体（OpenCC标准，旧字体）
LANGUAGE_CODE_TW2T = "tw2t.json"  # 繁体中文（台湾标准）到繁体中文台湾正体到繁体（OpenCC标准）
