# Y1 机器人信息
import os

Y1 = {"packageName": 'com.eps.logistics.yone',  # app包名
      "activity": 'com.app.ui.activity.HomeActivity', }

# Y2 机器人信息
Y2 = {"packageName": 'com.eps.logistics.ytwo',  # app包名
      "activity": 'com.app.ui.activity.HomeActivity', }

# E2 机器人信息
E2 = {"packageName": 'com.example.administrator.websc',  # app包名
      "activity": 'com.app.ui.activity.HomeActivity', }
# Y2P_PAD 开放式箱体PAD信息
Y2P_PAD = {
    "packageName": 'com.eps.openbox.pad',
    "activity": 'com.eps.openbox.pad.ui.activity.HomeActivity',
}
# 设备信息
devicesName = os.popen('adb devices ').read().split('\n')[1].split('\t')[0]
packageName = ''
for package in os.popen('adb shell pm list packages').read().split('\n'):
    package = package.split('package:')[-1]
    if package in [Y1["packageName"], Y2["packageName"], E2["packageName"], Y2P_PAD["packageName"]]:
        packageName = package
        break
# if packageName == '':
#     raise print("没有安装这个app或者没有连接这个机器")

# app登录账号
appUser = 'yunji123'
appPassword = '123456'

# app信息
qq_desired_caps = {
    "platformName": "Android",  # app安装的系统
    "platformVersion": "5.1.1",  # 系统版本
    "deviceName": "127.0.0.1:21503",  # 连接手机的名字在cmd 用adb devices -l查询
    'app': r'F:\qqlite_3.7.1.704_android_r110206_GuanWang_537057973_release_10000484.apk',  # app在win上的位置
    "appPackage": "com.tencent.qqlite",  # app在手机里面的名字
    # app的Activity 用aapt dump badging xxx.apk | find "launchable-activity"查找
    "appActivity": "com.tencent.mobileqq.activity.SplashActivity",
    "noReset": True
}
kaoyan_desired_caps = {
    "platformName": "Android",
    "platformVersion": "5.1.1",
    "deviceName": "127.0.0.1:21503",
    # 'app':r'C:\Users\dell\Desktop\kaoyanbang_3.4.1.251.apk',
    "appPackage": "com.tal.kaoyan",
    "appActivity": "com.tal.kaoyan.ui.activity.SplashActivity",
    "noReset": True
}
eps_desired_caps = {
    "platformName": "Android",
    "platformVersion": "5.1.1",
    "deviceName": "192.168.12.101:5006",  # "192.168.12.102:5555"
    # 'app':r'C:\Users\dell\Desktop\kaoyanbang_3.4.1.251.apk',
    "appPackage": "com.eps.logistics.yone",
    "appActivity": "com.app.ui.activity.LoginActivity",
    # "noReset": True
}
#
