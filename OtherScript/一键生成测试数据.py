import time
import unittest
from ddt import ddt, data
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
testCaseData = GetData(string_ele="name=任务与范围监控", TableName='预警管理')


@ddt
class MissionScopeWarning(unittestWeb):

    @classmethod
    def setUpClass(cls) -> None:
        cls.br = cls().login()

    @classmethod
    def tearDownClass(cls) -> None:
        # Clear_environment()  # 测试用例跑完清理测试环境
        cls().br.exitBrowser()
        print("完成")

    def setUp(self) -> None:  # 前置条件
        # Clear_environment()  # 清理web环境
        self.br.onClick('xpath=//span[text()="面板"]/../div/i')
        time.sleep(1)

    def tearDown(self) -> None:  # 还原测试环境
        time.sleep(1)
        self.br.onClick('xpath=//span[text()="面板"]/../div/i')
        print('执行完成')

    # @data(*testCaseData)
    # def test_MissionScopeWarning_add(self, key):
    #     # 进入添加页面
    #     self.epsAdd("任务与范围监控")
    #     # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
    #     Data = testCaseData[key]  # 是一个字典
    #     self.epsSelectClickContain(fieldName="机器人", labelName=Data['机器人'])
    #     self.epsSelectClick(fieldName="选择监控类型(默认为任务监控)", labelName=Data['选择监控类型(默认为任务监控)'])
    #     self.epsWrite(fieldName="输入范围半径(米)", value=Data['输入范围半径(米)'])
    #     self.epsSelectClick(fieldName="机器人状态(可多选)", labelName=Data['机器人状态(可多选)'])
    #     self.epsSelectClick(fieldName="选择预警级别(默认为中级)", labelName=Data['选择预警级别(默认为中级)'])
    #     self.epsWrite(fieldName='输入异常状态时间间隔(秒)', value=Data['输入异常状态时间间隔(秒)'])
    #     self.epsWrite(fieldName='输入异常推送次数(默认推送1次)', value=Data['输入异常推送次数(默认推送1次)'])
    #     # 添加页面提交
    #     self.epsSubmit()
    #     # 断言判断
    #     self.doAssert()

    # Data = ['机器人预警配置管理', '小程序用户管理', '语音审核管理', 'PC端管理', '工单管理', '故障统计', '工单故障类型', '工单问题类型',
    #         '消毒任务', '消毒任务配置', '机器人用户权限管理']
    #
    # @data(*Data)
    # def test_MissionScopeWarning_change(self, key):
    #     self.goToPage(key)
    #     try:
    #         for i in range(1000):
    #             ele_list = self.br.findElements('xpath=//tbody')[0].find_elements_by_xpath('tr/td[2]/div')
    #             for ele in ele_list:
    #                 if ele.text == "":
    #                     ele.find_element_by_xpath('../../td[1]').click()
    #             self.br.onClick('xpath=//button[@title="删除"]')
    #             ele = self.br.findElements('xpath=//span[text()="确定"]')
    #             if len(ele) == 2:
    #                 try:
    #                     ele[1].click()
    #                 except:
    #                     ele[0].click()
    #             else:
    #                 ele[0].click()
    #             time.sleep(0.5)
    #     except:
    #         print("完成")
    s = [['Y1Y2调度', 'y1y2y101', 'y1Y2调度y101', 'Y1(云际)',9],
         ['Y1Y2调度', 'y1y2y102', 'y1Y2调度y102', 'Y1(云际)',10],
         ['Y1Y2调度', 'y1y2y201', 'y1Y2调度y201', 'Y2(云际)',11],
         ['Y1Y2调度', 'y1y2y202', 'y1Y2调度y202', 'Y2(云际)',12],
         ['Y1E2调度', 'y1e2y101', 'Y1E2调度y101', 'Y1(云际)',13],
         ['Y1E2调度', 'y1e2y102', 'Y1E2调度y102', 'Y1(云际)',14],
         ['Y1E2调度', 'y1e2e201', 'Y1E2调度e201', 'E2(Mir底盘)',15],
         ['Y1E2调度', 'y1e2e202', 'Y1E2调度e202', 'E2(Mir底盘)',16]]

    @data(*s)
    def test_add_robot(self, data):
        print(data)
        self.goToAddPage('物流机器人管理')
        self.br.onClick('xpath=//label[text()="所属机构"]/../div/div')
        self.br.onClick('xpath=//label[text()="所属机构"]/../div/div/div[2]/ul[2]//span[text()="%s"]'%data[0])
        self.epsWrite(fieldName='名称', value=data[2])
        self.epsWrite(fieldName='序号', value=data[4])
        self.epsSelectClick(fieldName='型号', labelName=data[3])
        self.epsWrite(fieldName='登录账号', value=data[1])
        self.epsWrite(fieldName='登录密码', value='123456')
        self.epsSubmit()


if __name__ == "__main__":
    unittest.main()
"""
需要生成的配置信息
"""
