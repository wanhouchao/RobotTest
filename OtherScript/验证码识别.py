# -*- coding: utf-8 -*-
import json
import requests
import base64
from io import BytesIO
from PIL import Image
from sys import version_info


def base64_api(uname, pwd, img):
    img = img.convert('RGB')
    buffered = BytesIO()
    img.save(buffered, format="JPEG")
    if version_info.major >= 3:
        b64 = str(base64.b64encode(buffered.getvalue()), encoding='utf-8')
    else:
        b64 = str(base64.b64encode(buffered.getvalue()))
    data = {"username": uname, "password": pwd, "image": b64}
    result = json.loads(requests.post("http://api.ttshitu.com/base64", json=data).text)
    if result['success']:
        return result["data"]["result"]
    else:
        return result["message"]


def getImg():
    import re

    import requests

    headers = {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"
    }
    r = requests.get(url='https://test-tlcj.tuoluocaijing.cn/signin.html', headers=headers)
    # print(r.text)
    src = str(re.findall(r'\"getImgCode\(\'img_code_box\'\)\" src=\".*?\"', r.text)[0]).split('\"')[-2]
    url = 'https://test-tlcj.tuoluocaijing.cn' + src
    with open("picture_yanzhengma.jpg", "wb") as f:
        r = requests.get(url, headers=headers)
        f.write(r.content)
        f.close()


if __name__ == "__main__":
    getImg()
    img_path = "picture_yanzhengma.jpg"
    img = Image.open(img_path)
    result = base64_api(uname='Super_admin123', pwd='admin123', img=img)
    print(result)
