import os
import time


# dr = UiAutomatorFrame()  # 定义一个app驱动
#
# # # 关闭app
# dr.exitApp()

def killApp():
    sys = os.popen('adb shell "ps | grep com.eps.logistics.ytwo"').read()
    Pid = [pid for pid in sys.split('\n')[0].split(' ') if pid != ''][1]
    print('进程号:', Pid)
    os.system("	adb shell kill " + Pid)
    print("完成")
    time.sleep(10)
    sys = os.popen('adb shell "ps | grep com.eps.logistics.ytwo"').read()
    newPid = [pid for pid in sys.split('\n')[0].split(' ') if pid != ''][1]
    if Pid != newPid:
        print('进程号:',newPid)
        print("app已经启动")
        return True
    else:
        print('启动失败:')
        return False


def killGuard():
    sys = os.popen('adb shell "ps | grep com.eps.guard"').read()
    Pid1 = [pid for pid in sys.split('\n')[0].split(' ') if pid != ''][1]
    Pid2 = [pid for pid in sys.split('\n')[1].split(' ') if pid != ''][1]
    print("进程号:", Pid1, Pid2)
    os.system("	adb shell kill {}  {}".format(Pid1, Pid2))
    print("完成")
    time.sleep(5)
    sys = os.popen('adb shell "ps | grep com.eps.guard"').read()
    newPid1 = [pid for pid in sys.split('\n')[0].split(' ') if pid != ''][1]
    newPid2 = [pid for pid in sys.split('\n')[1].split(' ') if pid != ''][1]
    if Pid1 != newPid1 and Pid2 != newPid2:
        print("进程号:", newPid1, newPid2)
        print("守护app已经启动")
        return True
    else:
        print("启动失败")
        return False


if __name__ == '__main__':
    killApp()
    killGuard()
