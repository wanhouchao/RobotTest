from Comm.BaseFrame.Log import MyLog
from Comm.RobotApi.BaseApi import BaseApi, apiHeaders
from Comm.RobotApi.UnittestApi import unittestApi
from Comm.RobotWeb.SeleniumFramework import SeleniumFramework


def addCart():
    # 添加购物车
    url = 'https://www.debug.8591.com.hk/api/v3/card/generate-order-goods'
    goodsInfo = {'card_id': "2", 'denom_id': "18", 'quantity': 1}
    response = BaseApi().POST(url=url, json=goodsInfo, headers=apiHeaders).json()
    print("添加购物车-->{}".format(response))
    MyLog.info("添加购物车-->{}".format(response))
    return response


def placeTheOrder(goodsId):
    """
    生成订单
    :return:
    """
    orderInfo = {
        'goods': '[{"id": %d, "quantity": 1, "type": "orderGoods"}]' % goodsId,
        'payment_method': "hkpaypal",
        'payment_use_balance': 0,
    }
    url = "https://www.debug.8591.com.hk/api/v3/order/place-the-order"
    # 下单
    response = BaseApi().POST(url=url, json=orderInfo, headers=apiHeaders).json()
    print("下单-->{}".format(response))
    MyLog.info("下单-->{}".format(response))
    return response


def hkPayPal(userData):
    # 获取登录的cookie
    unittestApi().webGetToken(jsonData=userData)
    # 添加购物车
    goodsId = addCart()["data"]["id"]
    # 生成订单
    orderInfo = placeTheOrder(goodsId=goodsId)["data"]
    paymentUrl = orderInfo["payment_url"]
    orderNumber = orderInfo["order_no"]
    # 付款现在只能自己手动付款
    MyLog.info("订单编号：{},支付页面：{}".format(orderNumber, paymentUrl))

    # # 登录后台
    # br = SeleniumFramework()
    # br.openUrl("https://www.debug.8591.com.hk/")
    # response = unittestApi().webGetToken(jsonData=userData)
    # cookie = {'name': 'access_token_develop', 'value': response["data"]["access_token"]}
    # br.addCookies([cookie])
    # br.openUrl('https://www.debug.8591.com.hk/my/orders/detail?role=buyer&id=%s' % orderNumber)
    # br.onClick("xpath=//div[text()=\"立即支付\"]")
    # br.onClick("xpath=//input[@value=\"继续\"]")


if __name__ == '__main__':
    user = {
        "mobile": "84034704",  # 用户
        "password": "123456a",  # 用户
        "user_id": "1084181",  # 用户
    }
    # hkPayPal(userData=user)
    unittestApi().webGetToken(jsonData=user)
    orders = ["370651075451174181",
              "620651075382759181"]
    for order in orders:
        data = {
            'cancel_reason_code': "1",
            'order_no': order
        }
        response =BaseApi().POST(url="https://www.debug.8591.com.hk/api/v3/order/delete-order", json=data,
                       headers=apiHeaders).json()
        print(response)
