from time import sleep

from Comm.RobotWeb.SeleniumFramework import SeleniumFramework
from Config.WebConfig.test_epsit import Robot_url

br = SeleniumFramework()
br.openUrl(Robot_url)
br.doWrite('css=input[placeholder="用户名"]', 'admin')
br.doWrite('css=input[placeholder="密码"]', 'adminadmin')
br.onClick('xpath=//button[contains(.,"登录")]')

# 点击功能名称
br.onClick(
    'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="%s"]/..' % functionName)
# 进入添加编辑页面选择所属机构默认是自动化测试公司
br.onClick('css=button[title="增加"]')
br.onClick('xpath=//label[text()="所属机构"]/../div/div')
br.onClick('xpath=//li[@class="ivu-select-item"]/span[text()="自动化测试公司"]')
sleep(0.5)
