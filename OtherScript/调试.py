# import uiautomator2  as u2
# import os
#
# # 获取设备名字
# deviceName = os.popen('adb devices').read().split('\n')[1].split('	')[0]
#
# device = u2.connect_usb(deviceName)
#
# # print(devices.app_info('com.tal.kaoyan'))
#
# # 启动app
# device.app_start('com.tal.kaoyan')
# # device.xpath('//*[@resource-id="com.tal.kaoyan:id/tip_commit"]').click()
#
# # #注册流程
# # device(resourceId="com.tal.kaoyan:id/login_register_text").click()
# # device(resourceId="com.tal.kaoyan:id/activity_register_username_edittext").send_keys('liujuj10086')
# # device(resourceId="com.tal.kaoyan:id/activity_register_password_edittext").send_keys('qwer.1234')
# # device(resourceId="com.tal.kaoyan:id/activity_register_email_edittext").send_keys('1554897004@qq.com')
# # device(resourceId="com.tal.kaoyan:id/activity_register_register_btn").click()
#
# # login
# device(text='请输入用户名').send_keys('liujujj10086')
# device(text='请输入密码').send_keys('qwer.1234')
# device(text='登录').click()
#
# # 退出app
# device.app_stop('com.tal.kaoyan')
##验证
# from Config.TestEnv import EpsPath
#
# print(EpsPath)

#
# # ddt调试
# import time
# import unittest
# from ddt import ddt, unpack, data
#
# # 获取测试数据
# from Comm.Base import Clear_environment
# from Comm.DateDriver import GetData
# from Comm.SeleniumFramework import SeleniumFramework
# from Config.test_epsit import Robot_url
#
# testCaseData = GetData(string_ele="name=门控管理", TableName='配件管理')
# #
# # # title = testCaseData.pop("title")
# # print(testCaseData)
#
#
# @ddt
# class DoorcControl(unittest.TestCase):
#     @classmethod
#     def setUpClass(cls) -> None:
#         cls.br = SeleniumFramework()
#         cls.br.openUrl(Robot_url)
#         cls.br.doWrite('css=input[placeholder="用户名"]', 'admin')
#         cls.br.doWrite('css=input[placeholder="密码"]', 'adminadmin')
#         cls.br.onClick('xpath=//button[contains(.,"登录")]')
#
#     @classmethod
#     def tearDownClass(cls) -> None:
#         Clear_environment()  # 测试用例跑完清理测试环境
#
#     def setUp(self) -> None:  # 前置条件
#         # Clear_environment()  # 清理web环境
#         self.br.onClick('xpath=//span[text()="面板"]/../div/i')
#         time.sleep(1)
#
#     def tearDown(self) -> None:  # 还原测试环境
#         time.sleep(1)
#         self.br.onClick('xpath=//span[text()="面板"]/../div/i')
#         print('执行完成')
#
#     def epsAdd(self, functionName):
#         '''进入添加页面，functionName--》门控管理'''
#         # 点击功能名称
#         self.br.onClick(
#             'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="%s"]/..' % functionName)
#         # 进入添加编辑页面选择所属机构默认是自动化测试公司
#         self.br.onClick('css=button[title="增加"]')
#         self.br.onClick('xpath=//label[text()="所属机构"]/../div/div')
#         self.br.onClick('xpath=//li[@class="ivu-select-item"]/span[text()="自动化测试公司"]')
#         time.sleep(0.5)
#
#     def epsSelectClick(self, fieldName, labelName):
#         '''
#         fieldName-->字段名，labelName-->标签名字
#         '''
#         self.br.onClick('xpath=//label[text()="{}"]/../div/div'.format(fieldName))
#         self.br.onClick('xpath=//label[text()="{}"]/../div/div/div[2]/ul[2]/li[text()="{}"]'.format(fieldName, labelName))
#         time.sleep(0.5)
#
#     def epsWrite(self, fieldName, value):
#         '''通过字段名称填写信息 fieldName--》楼层，value--》6'''
#         self.br.doWrite('xpath=//label[text()="%s"]/../div/div/input' % fieldName, value)
#
#     def doAssert(self):
#         # 添加断言
#         ExpectedResult = '操作成功'  # 可以使用数据驱动
#         try:
#             ActualResult = self.br.getText('xpath=//div[text()="操作成功"]')
#         except:
#             ActualResult = None
#         print('\n' + "实际结果：" + ActualResult + '\n' + "预期结果：" + ExpectedResult + "\n")
#         self.assertEqual(ActualResult, ExpectedResult, '对比结果不一致')
#
#     def submit(self):
#         '''点击提交按钮'''
#         self.br.onClick('xpath=//span[text()="提交"]/..')
#
#     @data(*testCaseData)
#     def test_DoorcControl_add(self, key):
#         '''测试ddt模块'''
#         self.epsAdd("门控管理")
#         # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
#         Data = testCaseData[key]  # 是一个字典
#         self.epsWrite(fieldName='楼层', value=Data['楼层'])
#         self.epsWrite(fieldName='名称', value=Data['名称'])
#         self.epsWrite(fieldName='x坐标', value=Data['x坐标'])
#         self.epsWrite(fieldName='y坐标', value=Data['y坐标'])
#         self.epsWrite(fieldName='序列号', value=Data['序列号'])
#         self.epsWrite(fieldName='开门距离（即距离门几米时，触发开门动作）', value=Data['开门距离（即距离门几米时，触发开门动作）'])
#         self.epsWrite(fieldName='延时时长：（单位秒）', value=Data['延时时长：（单位秒）'])
#         self.epsWrite(fieldName='间隔时长：（单位秒）', value=Data['间隔时长：（单位秒）'])
#
#
# if __name__ == '__main__':
#     unittest.main()


# a = "python,hello"
# # 字符串的切片操作
# print(a.split(",")[0])
# # 字符串的替换
# print(a.replace(",", "="))
# # 查询字符串长度
# print(len(a))
# # 转义符
# b = "python \"hello\""
# print(b)
# path = r"E:\python\nython.exe"
# print(path)
# # find查找
# print(a.find("n"))
#
# dataList1 = [1, 2, 3, 4, 5]
# dataList2 = ["a", 2, 3, 4, 5]
# dataList3 = ["a", {"2": 3}, (3, 4), 4, 5]
# dataList1[0] = 6
# # print(dataList1)
# #
# # data = (1, 2, 3, 4, 5)
# # print(data[0])
#
# # 新增元素
# dataList1.append("12313")
# print(dataList1)
# # 删除元素
# # del dataList1[1]
# dataList1.remove(6)
# print(dataList1)
# 一定要和单元测试框架一起用
# import unittest, os
# from ddt import ddt, data, unpack, file_data
#
# '''NO.1单组元素'''
#
# List = [{"name": "123"}, {"name": "456"}, {"name": "789"}]
# @ddt
# class Testwork(unittest.TestCase):
#
#     @data(*List)
#     def test_01(self, value):  # value用来接收data的数据
#         print(value.get("name"))
#
#
# if __name__ == '__main__':
#     unittest.main()


