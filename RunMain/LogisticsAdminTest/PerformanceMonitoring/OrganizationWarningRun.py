from Config.BaseConfig.BaseConfig import *
from Comm.TestFrame.HTMLTestRunner import *
from Comm.BaseFrame.SendMail import *

# 主文件夹路径
EpsitAutoTestPath = os.getcwd()[::-1].split("\\", 3)[-1][::-1]
# 定义测试用例目录
Test_case_path = EpsitAutoTestPath + '\\Testcase\\LogisticsAdminTest\\PerformanceMonitoring'  # getcwd当前目录
# 定义测试报告目录
Test_report_path = EpsitAutoTestPath + '\\Report\\LogisticsAdminTest\\PerformanceMonitoring'
FileNameList = ['OrganizationWarning', 'LowPowerDetection', 'ExceptionConnectionStatus', 'MissionScopeWarning']
# 邮件
mail = SendMail()
mail.Add_text('自动化测试报告', subject, theSender, addressee)

for FileName in FileNameList:
    Test_report_name = Test_report_path + "\\" + FileName + ".html"
    print(Test_report_path)
    # 搜索测试用例
    suit = unittest.defaultTestLoader.discover(Test_case_path, pattern=FileName + '.py')
    # print(suit)
    # 执行测试用例+写报告
    f = open(Test_report_name, 'wb')  # 创建文档并且以二进制写入
    HTMLTestRunner(stream=f, title='自动化测试报告').run(suit)
    f.close()  # 关闭文档
    # 发添加附件
    mail.Add_file(Test_report_name, FileName + '.html')

mail.Send(server, port, mailUser, mailPwd, addressee)
