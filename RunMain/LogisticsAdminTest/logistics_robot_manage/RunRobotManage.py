s = '''
        ┏┓　　　┏┓+ +
　　　┏┛┻━━━┛┻┓ + +
　　　┃　　　　　　　┃ 　
　　　┃　　　━　　　┃ ++ + + +
　　 ████━████ ┃+
　　　┃　　　　　　　┃ +
　　　┃　　　┻　　　┃
　　　┃　　　　　　　┃ + +
　　　┗━┓　　　┏━┛
　　　　　┃　　　┃　　　　　　　　　　　
　　　　　┃　　　┃ + + + +
　　　　　┃　　　┃
　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
　　　　　┃　　　┃
　　　　　┃　　　┃　　+　　　　　　　　　
　　　　　┃　 　　┗━━━┓ + +
　　　　　┃ 　　　　　　　┣┓
　　　　　┃ 　　　　　　　┏┛
　　　　　┗┓┓┏━┳┓┏┛ + + + +
　　　　　　┃┫┫　┃┫┫
　　　　　　┗┻┛　┗┻┛+ + + +代码无bug
'''
print(s)
from Comm.TestFrame.HTMLTestRunner import *
from Comm.BaseFrame.SendMail import *
# from Testcase.LogisticsAdminTest.RobotManage import *


#定义测试用例目录
Test_case_path =os.getcwd()[:-50]+'\\Testcase\\LogisticsAdminTest\\RobotManage'#getcwd当前目录
print(Test_case_path)
# 定义测试报告目录
Test_report_path = os.getcwd()[:-50]+'\\Report\\LogisticsAdminTest\\RobotManage'
Test_report_name = Test_report_path+"\\logistics_robot_manage_RobotTestReport.html"
# 搜索测试用例
suit = unittest.defaultTestLoader.discover(Test_case_path,pattern='robot_*.py')
# suit1 = unittest.defaultTestLoader.discover(Test_case_path,pattern='robot_position_manage.py')
# suit2= unittest.defaultTestLoader.discover(Test_case_path,pattern='robot_user_manage.py')
# 执行测试用例+写报告
f = open(Test_report_name,'wb') #创建文档并且以二进制写入
HTMLTestRunner(stream=f,title='自动化测试报告').run(suit)
# HTMLTestRunner(stream=f,title='自动化测试报告').run(suit1)
# HTMLTestRunner(stream=f,title='自动化测试报告').run(suit2)
# HTMLTestRunner(stream=f,title='自动化测试报告').run(suit1)
f.close()#关闭文档
# 发邮件
mail = SendMail()
mail.Add_text('自动化测试报告',Subject,From,To)
mail.Add_file(Test_report_name,'logistics_robot_manage_RobotTestReport.html')
mail.Send(server,port,MailUser,MailPwd,To_addr)