def runCase():
    import unittest
    from Comm.TestFrame.HTMLTestRunner import HTMLTestRunner
    from Config.BaseConfig.BaseConfig import projectPath

    # 定义测试用例目录
    print("projectPath-->{}".format(projectPath))
    Test_case_path = projectPath + '/Comm/TestCaseAssert/'
    # 测试报告目录
    Test_report_path = projectPath + '/Report/'
    suit = unittest.defaultTestLoader.discover(Test_case_path, pattern='*.py')
    print(suit.countTestCases())
    Test_report_name = Test_report_path + "/" + 'assertTest' + ".html"
    f = open(Test_report_name, 'wb')  # 创建文档并且以二进制写入
    HTMLTestRunner(stream=f, title='自动化测试报告').run(suit)
    f.close()  # 关闭文档


if __name__ == '__main__':
    runCase()
