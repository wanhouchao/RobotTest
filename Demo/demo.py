import unittest
from Comm.BaseFrame.Log import MyLog
from Comm.RobotApi.BaseApi import BaseApi
from Comm.RobotWeb.SeleniumFramework import SeleniumFramework


class demoApiTest(unittest.TestCase):
    """
    demo演示
    """

    def setUp(self) -> None:  # 前置条件
        print("开始执行")

    def tearDown(self) -> None:  # 还原测试环境
        print("执行结束")

    def test_demoApi(self):
        """调试代码"""
        # 请求demo接口会返回ok
        self.api = BaseApi()
        MyLog.info("demo初始化，开始请求接口")
        print("demo初始化，开始请求接口")
        self.response = self.api.GET("http://47.98.58.33:8000/demo/demoApiTest/").json()
        self.assertEqual(first="ok", second=self.response["message"])
        MyLog.info("接口请求结束！！接口数据-->{}".format(self.response))
        print("接口请求结束！！接口数据-->{}".format(self.response))

    def test_demoUi(self):
        """打开百度"""
        self.br = SeleniumFramework(browserPattern=False)
        self.br.openUrl("https://www.baidu.com/")
        url = self.br.driver.current_url
        print(url)
        self.br.exitBrowser()
        self.assertEqual("https://www.baidu.com/", url)
        MyLog.info("ui测试结束！！实际结果-->{}".format(url))
        print("ui测试结束！！实际结果-->{}".format(url))


if __name__ == '__main__':
    print("",unittest.BaseTestSuite)
