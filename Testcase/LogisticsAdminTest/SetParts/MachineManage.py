import unittest
import time
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData

# 获取测试数据
from Comm.RobotWeb.UnittestWeb import unittestWeb
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=前置服务器管理", TableName='配件管理')


@ddt
# 继承Unittest里面自定义的方法
class MachineManage(logisticsInit):

    @data(*testCaseData)
    def test_MachineManage_add(self, key):
        """测试ddt模块"""
        # 进入添加页面
        self.epsAdd("前置服务器管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName='账号', value=Data['账号'])
        self.epsWrite(fieldName='密码', value=Data['密码'])
        # self.epsWrite(fieldName='公网IP', value=Data['公网IP'])
        # self.epsWrite(fieldName='流量卡号', value=Data['流量卡号'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_MachineManage_change(self):
        # 进入修改页面
        self.epsChange('前置服务器管理', '132654')
        # 修改选项
        self.epsWrite(fieldName='账号', value='132454')
        self.epsWrite(fieldName='密码', value='12313')
        # self.epsWrite(fieldName='公网IP', value="")
        # self.epsWrite(fieldName='流量卡号', value="321231")
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_MachineManage_delete(self):
        # 点击删除
        self.epsDelete("前置服务器管理", '132454')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
