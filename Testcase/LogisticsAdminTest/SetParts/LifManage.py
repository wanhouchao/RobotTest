import time
import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=电梯管理", TableName='配件管理')


@ddt
class LifManage(logisticsInit):


    @data(*testCaseData)
    def test_LifManage_add(self, key):
        # 进入添加页面
        self.epsAdd("电梯管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClick(fieldName="楼宇", labelName=Data['楼宇'])
        # self.epsSelectClick(fieldName="前置服务器", labelName=Data['前置服务器'])
        self.epsWrite(fieldName="电梯IP", value=Data['电梯IP'])
        self.epsWrite(fieldName="蓝牙标签识别码", value=Data['蓝牙标签识别码'])
        self.epsWrite(fieldName="梯控模块序列号", value=Data['梯控模块序列号'])
        self.epsSelectClick(fieldName="模式", labelName=Data['模式'])
        self.epsSelectClick(fieldName="接管类型", labelName=Data['接管类型'])
        self.epsWrite(fieldName='外呼板SN', value=Data['外呼板SN'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_LifManage_change(self):
        # 进入添加页面
        self.epsChange("电梯管理", '197.1.1.1')
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        self.epsSelectClick(fieldName="楼宇", labelName="GG")
        # self.epsSelectClick(fieldName="前置服务器", labelName=Data['前置服务器'])
        self.epsWrite(fieldName="电梯IP", value="197.1.1.2")
        self.epsWrite(fieldName="蓝牙标签识别码", value="2")
        self.epsWrite(fieldName="梯控模块序列号", value="2")
        self.epsSelectClick(fieldName="模式", labelName="闲时接管")
        self.epsSelectClick(fieldName="接管类型", labelName="机器人接管")
        self.epsWrite(fieldName='外呼板SN', value="3")
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_LifManage_delete(self):
        # 点击删除
        self.epsDelete("电梯管理", '197.1.1.2')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
