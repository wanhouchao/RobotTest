import time
import unittest
# from ddt import ddt, unpack, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
# testCaseData = GetData(string_ele=r"name=背景音乐管理",TableName='/Data/LogisticsAdminTest/配件管理/配件管理.xlsx')
# print(testCaseData)
from Config.BaseConfig.BaseConfig import projectPath
from Testcase.LogisticsAdminTest import logisticsInit


class BgmManage(logisticsInit):

    def test_Bgm_add(self):
        self.epsAdd("背景音乐管理", '归属机构')
        self.epsWrite(fieldName='背景音乐名称', value='自动化测试脚本')
        self.br.uploadFile('xpath=//span[text()="文件上传"]/..', (projectPath + r'/Config/Bgm/Mp3.mp3').replace("/", "\\"))
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_Bgm_delete(self):
        # 点击删除
        self.epsDelete("背景音乐管理", '自动化测试脚本')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
