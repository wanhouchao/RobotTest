import time
import unittest
# from ddt import ddt, unpack, data
from ddt import ddt, data

from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=门控管理", TableName='配件管理')


@ddt
class DoorControl(logisticsInit):



    @data(*testCaseData)
    def test_DoorcControl_add(self, key):
        '''测试ddt模块'''
        # 进入添加页面
        self.epsAdd("门控管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName='楼层', value=Data['楼层'])
        self.epsWrite(fieldName='名称', value=Data['名称'])
        self.epsWrite(fieldName='x坐标', value=Data['x坐标'])
        self.epsWrite(fieldName='y坐标', value=Data['y坐标'])
        self.epsWrite(fieldName='序列号', value=Data['序列号'])
        self.epsWrite(fieldName='开门距离（即距离门几米时，触发开门动作）', value=Data['开门距离（即距离门几米时，触发开门动作）'])
        self.epsWrite(fieldName='延时时长：（单位秒）', value=Data['延时时长：（单位秒）'])
        self.epsWrite(fieldName='间隔时长：（单位秒）', value=Data['间隔时长：（单位秒）'])
        self.epsSelectClick(fieldName="门控类型", labelName=Data['门控类型'])
        self.epsSelectClick(fieldName="按键类型", labelName=Data['按键类型'])
        self.epsSelectClick(fieldName="楼宇", labelName=Data['楼宇'])
        self.epsSelectClickContain(fieldName="机器人类型", labelName=Data['机器人类型'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_DoorcControl_change(self):
        # 进入修改页面
        self.epsChange('门控管理', 'robottest')
        # 修改选项
        self.epsWrite(fieldName='楼层', value='2')
        self.epsWrite(fieldName='名称', value='robottest')
        self.epsWrite(fieldName='x坐标', value='1')
        self.epsWrite(fieldName='y坐标', value='1')
        self.epsWrite(fieldName='序列号', value='1')
        self.epsWrite(fieldName='开门距离（即距离门几米时，触发开门动作）', value='1')
        self.epsWrite(fieldName='延时时长：（单位秒）', value='1')
        self.epsWrite(fieldName='间隔时长：（单位秒）', value='1')
        self.epsSelectClick(fieldName="门控类型", labelName='贴近电梯')
        self.epsSelectClick(fieldName="按键类型", labelName='长按')
        self.epsSelectClick(fieldName="机器人类型", labelName='Y2(云际)')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_DoorcControl_delete(self):
        # 点击删除
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="门控管理"]/..')
        self.br.onClick('css=span[data-v-6f699979]')
        self.br.onClick('xpath=//span[text()="自动化测试公司"][@class="ant-select-tree-title"]')
        time.sleep(0.5)

        self.br.findElements('xpath=//div[text()="robottest"]/../..//div/a[@title="删除"]/i')[1].click()

        self.br.findElements('css=button[class="ivu-btn ivu-btn-primary ivu-btn-large"]')[0].click()
        time.sleep(0.5)
        # 添加断言
        self.doAssert()

if __name__ == '__main__':
    unittest.main()
