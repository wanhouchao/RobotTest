import time
import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb
from Comm.DataDrive.DataDriver import GetData

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=通话模块管理", TableName='配件管理')


# print(testCaseData)

@ddt
class Conversation(logisticsInit):

    @data(*testCaseData)
    def test_Conversation_add(self, key):
        # 进入添加页面
        self.epsAdd("通话模块管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        # self.epsSelectClick(fieldName='前置服务器', labelName='132654132454')
        self.epsWrite(fieldName='内网ip', value=Data['内网ip'])
        self.epsWrite(fieldName='SIP账号', value=Data['SIP账号'])
        self.epsWrite(fieldName='SIP密码', value=Data['SIP密码'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_Conversation_change(self):
        # 进入修改页面
        self.epsChange('通话模块管理', 'wuyanzu')
        # 修改选项
        self.epsWrite(fieldName='SIP账号', value='6666')
        self.epsWrite(fieldName='SIP密码', value='pengyuyan')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_Conversation_delete(self):
        # 点击删除
        self.epsDelete("通话模块管理", 'pengyuyan')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
