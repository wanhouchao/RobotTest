import time
import unittest
# from ddt import ddt, unpack, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
# testCaseData = GetData(string_ele=r"name=电梯管理", TableName='/Data/LogisticsAdminTest/配件管理/配件管理.xlsx')
# print(testCaseData)
from Testcase.LogisticsAdminTest import logisticsInit


class MobileTerminal(logisticsInit):

    def test_MobileTerminal_add(self):
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="移动终端管理"]/..')
        self.br.onClick('css=button[title="增加"]')
        self.br.onClick('xpath=//label[text()="所属机构"]/../div/div')
        self.br.onClick('xpath=//li[@class="ivu-select-item"]/span[text()="自动化测试公司"]')
        time.sleep(0.5)
        self.epsSelectClick('选择机器人类型', 'Y1(云际)')
        self.epsSelectClickContain('绑定位置', '我的位置')
        self.br.doWrite('xpath=//label[text()="编码"]/../div/div/input', '1988')
        self.br.doWrite('xpath=//label[text()="名称"]/../div/div/input', '自动化测试')

        self.br.onClick('xpath=//span[text()="提交"]/..')

        # 添加断言
        self.doAssert()

    def test_MobileTerminal_change(self):
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="移动终端管理"]/..')
        self.br.onClick('css=span[data-v-6f699979]')
        self.br.onClick('xpath=//span[text()="自动化测试公司"][@class="ant-select-tree-title"]')
        time.sleep(0.5)

        ele = self.br.driver.find_elements_by_xpath('//div[text()="自动化测试公司"]/../../td[8]/div/a[@title="修改"]')
        ele[1].click()

        # self.epsSelectClick('选择机器人类型','Y1(云际)')
        # self.epsSelectClick('绑定位置','')
        self.br.doWrite('xpath=//label[text()="编码"]/../div/div/input', '1988')
        self.br.doWrite('xpath=//label[text()="名称"]/../div/div/input', '请回答1988')

        self.br.onClick('xpath=//span[text()="提交"]/..')
        # 添加断言
        self.doAssert()

    def test_MobileTerminal_delete(self):
        self.epsDelete('移动终端管理', '请回答1988')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
