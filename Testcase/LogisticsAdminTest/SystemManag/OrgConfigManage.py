import time
import unittest
from ddt import ddt, data
from Comm.DataDrive.DataDriver import GetData
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=机构配置管理", TableName='系统管理')


@ddt
# 继承Unittest里面自定义的方法
class OrgConfigManage(logisticsInit):

    def tearDown(self) -> None:  # 还原测试环境
        time.sleep(1)
        self.clickPanel()
        print('执行完成')

    @data(*testCaseData)
    def test_OrgConfigManage_add(self, key):
        # 进入添加页面
        self.epsAdd("机构配置管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClickContain('指定机器人', 'liujj')
        self.epsWrite(fieldName='闲时回指定位置时间（秒）', value=Data['闲时回指定位置时间（秒）'])
        self.epsWrite(fieldName='核对物品时间（秒）', value=Data['核对物品时间（秒）'])
        self.epsSelectClick('运行模式', '单机模式')
        self.epsWrite(fieldName='免打扰时间', value=Data['免打扰时间'])
        self.epsWrite(fieldName='超级密码', value=Data['超级密码'])
        self.epsSelectClickContain('PC默认显示型号', 'E2')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_OrgConfigManage_change(self):
        # 进入修改页面
        self.epsChange('机构配置管理', 'liujj')
        # 修改选项
        self.epsSelectClickContain('指定机器人', 'wanhc')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_OrgConfigManage_delete(self):
        """这个比较特殊 这个是测试不能删除配置"""
        # 点击删除
        self.goToPage('机构配置管理')
        self.br.onClick('xpath=//div[text()="wanhc"]//..//..//span')
        # 添加断言
        time.sleep(1)
        ExpectedResult = ''  # 可以使用数据驱动
        try:
            self.br.onClick('css=button[title="删除"]')
            ActualResult = '操作成功'
        except:
            ActualResult = ''
        print('\n' + "实际结果：" + ActualResult + '\n' + "预期结果：" + ExpectedResult + "\n")
        self.assertEqual(ActualResult, ExpectedResult, '对比结果不一致')


if __name__ == '__main__':
    unittest.main()
