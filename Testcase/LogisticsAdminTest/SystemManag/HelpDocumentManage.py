import time
import unittest

from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb

# testCaseData = GetData(string_ele="name=帮助文档管理", TableName='系统管理')


# @ddt
# 继承Unittest里面自定义的方法
from Testcase.LogisticsAdminTest import logisticsInit


class HelpDocumentManage(logisticsInit):


    # @data(*testCaseData)
    def test_HelpDocumentManage_add(self):
        # 进入添加页面
        self.goToAddPage('帮助文档管理')
        self.epsWrite(fieldName='标题', value='测试测试')
        self.epsWrite(fieldName='说明', value='测试测试')
        self.epsSelectClick(fieldName='状态', labelName='new')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_HelpDocumentManage_change(self):
        # 进入修改页面
        self.goToPage('帮助文档管理')
        self.epsSearchClick('测试测试', '输入标题')
        # 修改选项
        self.epsWrite(fieldName='标题', value='自动化')
        self.epsWrite(fieldName='说明', value='自动化')
        self.epsSelectClick(fieldName='状态', labelName='hot')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_HelpDocumentManage_delete(self):
        # 点击删除
        self.goToPage('帮助文档管理')
        self.epsSearchClick('自动化', '输入标题', action='删除')
        self.br.onClick('xpath=//span[text()="确定"]')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
