import time
import unittest
from ddt import ddt, data
from Comm.DataDrive.DataDriver import GetData
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=用户管理", TableName='系统管理')


@ddt
# 继承Unittest里面自定义的方法
class UserManage(logisticsInit):

    def tearDown(self) -> None:  # 还原测试环境
        time.sleep(1)
        self.clickPanel()
        print('执行完成')

    @data(*testCaseData)
    def test_UserManage_add(self, key):
        # 进入添加页面
        self.epsAdd("用户管理", "所属机构")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName='姓名', value=Data['姓名'])
        self.epsWrite(fieldName='登录名', value=Data['登录名'])
        self.epsWrite(fieldName='密码', value=Data['密码'])
        self.epsWrite(fieldName='确认密码', value=Data['确认密码'])
        self.epsSelectClick(fieldName='用户角色', labelName='管理员')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_UserManage_change(self):
        # 进入修改页面
        self.epsChange('用户管理', 'wuyanzu')
        # 修改选项
        self.epsWrite(fieldName='姓名', value='姓名')
        self.epsWrite(fieldName='登录名', value='pengyuyan')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_UserManage_delete(self):
        # 点击删除
        self.epsDelete("用户管理", 'pengyuyan')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
