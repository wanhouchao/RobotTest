import time
import unittest
from ddt import ddt, data

from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=字典管理", TableName='系统管理')


@ddt
# 继承Unittest里面自定义的方法
class DictManage(logisticsInit):

    @data(*testCaseData)
    def test_DictManage_add(self, key):
        # 进入添加页面
        self.goToAddPage('字典管理')
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName='类型', value=Data['类型'])
        self.epsWrite(fieldName='类型描述', value=Data['类型描述'])
        self.epsWrite(fieldName='数据值', value=Data['数据值'])
        self.epsWrite(fieldName='标签', value=Data['标签'])
        self.epsSort(fieldName='排序', value=Data['排序'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_DictManage_change(self):
        # 进入修改页面
        self.goToPage('字典管理')
        self.epsClick('测试')
        # 修改选项
        self.epsWrite(fieldName='类型描述', value='自动化')
        self.epsWrite(fieldName='标签', value='自动化')
        self.epsWrite(fieldName='数据值', value='112313')
        self.epsWrite(fieldName='标签', value='asda')
        self.epsSort(fieldName='排序', value='4654')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_DictManage_delete(self):
        # 点击删除
        self.goToPage('字典管理')
        self.epsClickAction('自动化', action='删除')
        self.br.onClick('xpath=//span[text()="确定"]')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
