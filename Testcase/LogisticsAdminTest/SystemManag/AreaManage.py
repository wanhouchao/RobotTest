import time
import unittest

from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb


# testCaseData = GetData(string_ele="name=区域管理", TableName='系统管理')


# @ddt
# 继承Unittest里面自定义的方法
from Testcase.LogisticsAdminTest import logisticsInit


class AreaManage(logisticsInit):

    # @data(*testCaseData)
    def test_AreaManage_add(self):
        # 进入添加页面
        self.goToAddPage('区域管理')
        self.epsWrite(fieldName='名称', value='测试')
        self.epsWrite(fieldName='编码', value='518114')
        self.epsSelectClick(fieldName='类别', labelName='其它')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_AreaManage_change(self):
        # 进入修改页面
        self.goToPage('区域管理')
        self.epsSearchClick('测试','输入名称',)
        # 修改选项
        self.epsWrite(fieldName='名称', value='自动化')
        self.epsWrite(fieldName='编码', value='518114')
        self.epsSelectClick(fieldName='类别', labelName='国家')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_AreaManage_delete(self):
        # 点击删除
        self.goToPage('区域管理')
        self.epsSearchClick('自动化','输入名称',action='删除')
        self.br.onClick('xpath=//span[text()="确定"]')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
