import time
import unittest

from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb


# testCaseData = GetData(string_ele="name=角色管理", TableName='系统管理')


# @ddt
# 继承Unittest里面自定义的方法
from Testcase.LogisticsAdminTest import logisticsInit


class RoleManage(logisticsInit):

    # @data(*testCaseData)
    def test_RoleManage_add(self):
        # 进入添加页面
        self.goToAddPage('角色管理')
        self.epsWrite(fieldName='角色名称', value='自动化')
        self.epsWrite(fieldName='备注', value='自动化脚本')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RoleManage_change(self):
        # 进入修改页面
        self.goToPage('角色管理')
        self.epsClickAction('自动化脚本', action='修改')
        # 修改选项
        self.epsWrite(fieldName='角色名称', value='自动化')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RoleManage_delete(self):
        # 点击删除
        self.goToPage('角色管理')
        self.epsClickAction(f'自动化脚本', action='删除')
        self.br.onClick('xpath=//span[text()="确定"]')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
