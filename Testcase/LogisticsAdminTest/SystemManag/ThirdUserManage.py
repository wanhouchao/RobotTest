import time
import unittest

from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb


# testCaseData = GetData(string_ele="name=第三方用户管理", TableName='系统管理')


# @ddt
# 继承Unittest里面自定义的方法
from Testcase.LogisticsAdminTest import logisticsInit


class ThirdUserManage(logisticsInit):

    # @data(*testCaseData)
    def test_ThirdUserManage_add(self):
        # 进入添加页面
        self.epsAdd('第三方用户管理')
        self.epsWrite(fieldName='用户名', value='wuyanzu')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ThirdUserManage_change(self):
        # 进入修改页面
        self.goToPage('第三方用户管理')
        self.epsSearchClick('wuyanzu','输入用户名')
        # 修改选项
        self.epsWrite(fieldName='用户名', value='pengyuyan')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ThirdUserManage_delete(self):
        # 点击删除
        self.goToPage('第三方用户管理')
        self.epsSearchClick('pengyuyan','输入用户名',action='删除')
        self.br.onClick('xpath=//span[text()="确定"]')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
