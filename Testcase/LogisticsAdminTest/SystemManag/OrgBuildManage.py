# import time
# import unittest
# from ddt import ddt, unpack, data
#
# from Comm.Base import Clear_environment
# from Comm.DateDriver import GetData
# from Comm.SeleniumFramework import SeleniumFramework
# from Comm.UnittestFramework import Unittest
#
# from Config.test_epsit import Robot_url
#
# testCaseData = GetData(string_ele="name=机构楼宇管理", TableName='系统管理')
#
#
# @ddt
# # 继承Unittest里面自定义的方法
# class OrgBuildManage(Unittest):
#     @classmethod
#     def setUpClass(cls) -> None:
#         cls.br = cls().login()
#
#     @classmethod
#     def tearDownClass(cls) -> None:
#         Clear_environment()  # 测试用例跑完清理测试环境
#
#     def setUp(self) -> None:  # 前置条件
#         # Clear_environment()  # 清理web环境
#         self.clickPanel()
#         time.sleep(1)
#
#     def tearDown(self) -> None:  # 还原测试环境
#         time.sleep(1)
#         self.clickPanel()
#         print('执行完成')
#
#     @data(*testCaseData)
#     def test_OrgBuildManage_add(self, key):
#         # 进入添加页面
#         self.epsAdd("机构楼宇管理")
#         # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
#         Data = testCaseData[key]  # 是一个字典
#         self.epsWrite(fieldName='名称', value=Data['名称'])
#         self.epsWrite(fieldName='简称', value=Data['简称'])
#         # 添加页面提交
#         self.epsSubmit()
#         # 断言判断
#         self.doAssert()
#
#     def test_OrgBuildManage_change(self):
#         # 进入修改页面
#         self.epsChange('机构楼宇管理', 'SG')
#         # 修改选项
#         self.epsWrite(fieldName='名称', value='MM')
#         self.epsWrite(fieldName='简称', value='MM')
#         # 添加页面提交
#         self.epsSubmit()
#         # 断言判断
#         self.doAssert()
#
#     def test_OrgBuildManage_delete(self):
#         # 点击删除
#         self.epsDelete("机构楼宇管理", 'MM')
#         # 添加断言
#         self.doAssert()
#
#
# if __name__ == '__main__':
#     unittest.main()
