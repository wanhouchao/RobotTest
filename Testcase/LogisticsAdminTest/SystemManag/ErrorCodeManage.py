import unittest
# testCaseData = GetData(string_ele="name=错误码管理", TableName='系统管理')


# @ddt
# 继承Unittest里面自定义的方法
from Testcase.LogisticsAdminTest import logisticsInit


class ErrorCodeManage(logisticsInit):

    # @data(*testCaseData)
    def test_ErrorCodeManage_add(self):
        # 进入添加页面
        self.goToAddPage('错误码管理')
        self.epsWrite(fieldName='错误码', value='666666')
        self.epsWrite(fieldName='错误消息', value='测试')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ErrorCodeManage_change(self):
        # 进入修改页面
        self.goToPage('错误码管理')
        self.epsSearchClick('666666', '输入错误码')
        # 修改选项
        self.epsWrite(fieldName='错误码', value='999999')
        self.epsWrite(fieldName='错误消息', value='自动化')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ErrorCodeManage_delete(self):
        # 点击删除
        self.goToPage('错误码管理')
        self.epsSearchClick('999999', '输入错误码', action='删除')
        self.br.onClick('xpath=//span[text()="确定"]')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
