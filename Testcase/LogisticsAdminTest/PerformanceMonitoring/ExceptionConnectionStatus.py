import time
import unittest
# from ddt import ddt, unpack, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb
from Testcase.LogisticsAdminTest import logisticsInit


class ExceptionConnectionStatus(logisticsInit):


    def test_ExceptionConnectionStatus_add(self):
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="异常与连接状态监控"]/..')
        self.br.onClick('css=button[title="增加"]')
        self.br.onClick('xpath=//label[text()="所属机构"]/../div/div')
        self.br.onClick('xpath=//span[text()="自动化测试公司"]')
        time.sleep(0.5)
        self.br.onClick('xpath=//label[text()="机器人状态"]/../div/div')
        self.br.onClick('xpath=//li[text()="空闲中"]')
        time.sleep(0.5)
        self.br.doWrite('xpath=//label[text()="输入异常状态时间间隔(秒)"]/../div/div/input', '12')
        self.br.doWrite('xpath=//label[text()="输入异常推送次数(默认推送1次)"]/../div/div/input', '1')
        self.br.onClick('xpath=//label[text()="选择预警级别(默认为中级)"]/../div/div')
        self.br.onClick('xpath=//li[text()="低"]')
        time.sleep(0.5)
        self.br.doWrite('xpath=//label[text()="备注信息"]/../div/div/textarea', '自动化测试脚本')
        self.br.onClick('xpath=//span[text()="提交"]/..')
        # 添加断言
        self.doAssert()

    def test_ExceptionConnectionStatus_change(self):
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="异常与连接状态监控"]/..')
        self.br.onClick('css=span[data-v-6f699979]')
        self.br.onClick('xpath=//span[text()="自动化测试公司"][@class="ant-select-tree-title"]')
        time.sleep(0.5)

        ele = self.br.driver.find_elements_by_xpath('//div[text()="空闲中"]/../../td[9]/div/a[@title="修改"]')
        ele[1].click()

        self.br.onClick('xpath=//label[text()="机器人状态"]/../div/div')
        self.br.onClick('xpath=//li[text()="离线状态"]')
        time.sleep(0.5)
        self.br.doWrite('xpath=//label[text()="输入异常状态时间间隔(秒)"]/../div/div/input', '6')
        self.br.doWrite('xpath=//label[text()="输入异常推送次数(默认推送1次)"]/../div/div/input', '3')
        self.br.onClick('xpath=//label[text()="选择预警级别(默认为中级)"]/../div/div')
        self.br.onClick('xpath=//li[text()="高"]')
        time.sleep(0.5)
        self.br.doWrite('xpath=//label[text()="备注信息"]/../div/div/textarea', '自动化测试脚本')
        self.br.onClick('xpath=//span[text()="提交"]/..')
        # 添加断言
        self.doAssert()

    def test_ExceptionConnectionStatus_delete(self):
        self.br.onClick(
            'xpath=//button[@class="el-button el-button--default el-button--small"]/span[text()="异常与连接状态监控"]/..')
        self.br.onClick('css=span[data-v-6f699979]')
        self.br.onClick('xpath=//span[text()="自动化测试公司"][@class="ant-select-tree-title"]')
        time.sleep(0.5)

        ele = self.br.driver.find_elements_by_xpath('//div[text()="离线状态"]/../../td[9]/div/a[@title="删除"]')
        ele[1].click()

        self.br.onClick('css=button[class="ivu-btn ivu-btn-primary ivu-btn-large"]')
        time.sleep(0.5)
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
