import time
import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=低电量检测", TableName='预警管理')


@ddt
class LowPowerDetection(logisticsInit):

    @data(*testCaseData)
    def test_LowPowerDetection_add(self, key):
        # 进入添加页面
        self.epsAdd("低电量检测")
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName='输入电量(%)', value=Data['输入电量(%)'])
        self.epsWrite(fieldName='输入异常推送次数(默认推送1次)', value=Data['输入异常推送次数(默认推送1次)'])
        self.epsSelectClick(fieldName="选择预警级别(默认为中级)", labelName=Data['选择预警级别(默认为中级)'])
        self.epsWrite(fieldName='备注信息', value=Data['备注信息'])
        # 页面提交
        self.epsSubmit()
        # 添加断言
        self.doAssert()

    def test_LowPowerDetection_change(self):
        # 进入修改页面
        self.epsChange('低电量检测', '自动化测试公司')
        # 修改选项
        self.epsWrite(fieldName='输入电量(%)', value='79')
        self.epsWrite(fieldName='输入异常推送次数(默认推送1次)', value='2')
        self.epsSelectClick(fieldName="选择预警级别(默认为中级)", labelName='高')
        self.epsWrite(fieldName='备注信息', value='')
        # 页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_LowPowerDetection_delete(self):
        # 点击删除
        self.epsDelete('低电量检测', '自动化测试公司')
        # 断言判断
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
