import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=任务与范围监控", TableName='预警管理')


@ddt
class MissionScopeWarning(logisticsInit):

    @data(*testCaseData)
    def test_MissionScopeWarning_add(self, key):
        # 进入添加页面
        self.epsAdd("任务与范围监控")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClickContain(fieldName="机器人", labelName=Data['机器人'])
        self.epsSelectClick(fieldName="选择监控类型", labelName=Data['选择监控类型'])
        self.epsWrite(fieldName="输入范围半径(米)", value=Data['输入范围半径(米)'])
        self.epsSelectClick(fieldName="机器人状态(可多选)", labelName=Data['机器人状态(可多选)'])
        self.epsSelectClick(fieldName="选择预警级别(默认为中级)", labelName=Data['选择预警级别(默认为中级)'])
        self.epsWrite(fieldName='输入异常状态时间间隔(秒)', value=Data['输入异常状态时间间隔(秒)'])
        self.epsWrite(fieldName='输入异常推送次数(默认推送1次)', value=Data['输入异常推送次数(默认推送1次)'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_MissionScopeWarning_change(self):
        # 进入修改页面
        self.epsChange('任务与范围监控', 'wanhc')
        # 修改选项
        self.epsSelectClickContain(fieldName="机器人", labelName='jinwei')
        self.epsSelectClick(fieldName="选择监控类型", labelName='任务监控')
        self.epsSelectClick(fieldName="选择预警级别(默认为中级)", labelName='中')
        self.epsSelectClick(fieldName="机器人状态(可多选)", labelName='配送中')
        self.epsWrite(fieldName='输入异常状态时间间隔(秒)', value='80')
        self.epsWrite(fieldName='输入异常推送次数(默认推送1次)', value='3')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_MissionScopeWarning_delete(self):
        # 点击删除
        self.epsDelete("任务与范围监控", 'jinwei')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
