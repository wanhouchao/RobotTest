import time
import unittest
from ddt import ddt, data
from Comm.DataDrive.DataDriver import GetData

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=预警配置", TableName='预警管理')


@ddt
class WarningConfig(logisticsInit):

    @data(*testCaseData)
    def test_WarningConfig_add(self, key):
        # 进入添加页面
        self.epsAdd("预警配置")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClick(fieldName="预警类型", labelName=Data['预警类型'])
        self.epsSelectClick(fieldName="预警级别", labelName=Data['预警级别'])
        self.epsSelectClick(fieldName="选择预警用户", labelName=Data['选择预警用户'])
        # 页面提交
        self.epsSubmit()
        # 添加断言
        self.doAssert()

    def test_WarningConfig_change(self):
        # 进入修改页面
        self.epsChange('预警配置', '自动化测试公司')
        # 修改选项
        self.epsSelectClick(fieldName="预警类型", labelName="不通知")
        self.epsSelectClick(fieldName="预警级别", labelName="高")
        self.epsSelectClick(fieldName="选择预警用户", labelName="魏刚1")
        # 页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_WarningConfig_delete(self):
        # 点击删除
        self.epsDelete('预警配置', '自动化测试公司')
        # 断言判断
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
