import unittest
from Testcase.LogisticsAdminTest import logisticsInit  # 初始化


# 获取测试数据
# testCaseData = GetData(string_ele="name=管制区域管理", TableName='调度管理')


class Control(logisticsInit):

    def test_Control_add(self):
        self.epsAdd("管制区域管理")
        self.epsSelectClick(fieldName='楼宇', labelName='A')
        self.epsWrite(fieldName='楼层 ', value='9')
        self.epsSelectClickContain(fieldName='选择机器人类型(默认为MIR)', labelName='Y2(云际)')
        self.epsWrite(fieldName='备注', value='自动化测试脚本')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_Control_change(self):
        # 进入修改页面
        self.epsChange('管制区域管理', '自动化测试脚本')
        # 修改选项
        # self.epsSelectClick(fieldName='楼宇', labelName='GG')
        # self.epsWrite(fieldName='楼层 ', value='5')
        self.epsSelectClickContain(fieldName='选择机器人类型(默认为MIR)', labelName='Y1(云际)')
        self.epsWrite(fieldName='备注', value='修改自动化')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_Control_delete(self):
        # 点击删除
        self.epsDelete("管制区域管理", '修改自动化')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
