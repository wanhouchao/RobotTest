import time
import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Testcase.LogisticsAdminTest import logisticsInit

# 获取测试数据
testCaseData = GetData(string_ele="name=坐标转换配置管理", TableName='调度管理')


@ddt
class LocationTransform(logisticsInit):
    """
    位置转换配置管理
    """

    @data(*testCaseData)
    def test_LocationTransform_add(self, key):
        # 进入添加页面
        self.epsAdd("坐标转换配置管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        print(Data)
        self.epsSelectClick(fieldName="楼宇", labelName=Data['楼宇'])
        self.epsSelectClick(fieldName="基准的机器人类型", labelName=Data['基准的机器人类型'])
        self.epsSelectClickContain(fieldName="映射的机器人类型", labelName=Data['映射的机器人类型'])
        self.epsWrite(fieldName="楼层", value=Data['楼层'])
        self.epsWrite(fieldName="位置坐标转换的参数(云际间转换可不填)", value=Data['位置坐标转换的参数(云际间转换可不填)'])
        self.epsWrite(fieldName="映射MIR地图高(云际间转换可不填)(像素)", value=Data['映射MIR地图高(云际间转换可不填)(像素)'])
        self.epsWrite(fieldName="映射MIR地图宽(云际间转换可不填)(像素)", value=Data['映射MIR地图宽(云际间转换可不填)(像素)'])
        self.epsWrite(fieldName="旋转角度", value=Data['旋转角度'])
        self.epsWrite(fieldName="映射地图高缩放比例(%)", value=Data['映射地图高缩放比例(%)'])
        self.epsWrite(fieldName="映射地图宽缩放比例(%)", value=Data['映射地图宽缩放比例(%)'])
        self.epsWrite(fieldName="x轴偏差位置(像素)", value=Data['x轴偏差位置(像素)'])
        self.epsWrite(fieldName="y轴偏差位置(像素)", value=Data['y轴偏差位置(像素)'])
        self.epsWrite(fieldName="备注", value=Data['备注'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_LocationTransform_change(self):
        # 进入添加页面
        self.epsChange("坐标转换配置管理", '自动化测试脚本')
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        self.epsSelectClick(fieldName="基准的机器人类型", labelName='Y1(云际)')
        self.epsSelectClickContain(fieldName="映射的机器人类型", labelName='Y2(云际)')
        self.epsWrite(fieldName="楼层", value='5')
        self.epsWrite(fieldName="位置坐标转换的参数(云际间转换可不填)", value='311')
        self.epsWrite(fieldName="映射MIR地图高(云际间转换可不填)(像素)", value='312')
        self.epsWrite(fieldName="映射MIR地图宽(云际间转换可不填)(像素)", value='313')
        self.epsWrite(fieldName="旋转角度", value='60')
        self.epsWrite(fieldName="映射地图高缩放比例(%)", value='22')
        self.epsWrite(fieldName="映射地图宽缩放比例(%)", value='33')
        self.epsWrite(fieldName="x轴偏差位置(像素)", value='11')
        self.epsWrite(fieldName="y轴偏差位置(像素)", value='11')
        self.epsWrite(fieldName="备注", value='修改自动化')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_LocationTransform_delete(self):
        # 点击删除
        self.epsDelete("坐标转换配置管理", '修改自动化')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
