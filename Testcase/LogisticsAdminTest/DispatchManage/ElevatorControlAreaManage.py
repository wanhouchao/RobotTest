import time
import unittest
from ddt import ddt, data
from Comm.DataDrive.DataDriver import GetData

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=电梯管制区域管理", TableName='调度管理')


@ddt
class ElevatorControlAreaManage(logisticsInit):
    """
    电梯管制区域管理
    """

    @data(*testCaseData)
    def test_ElevatorControlAreaManage_add(self, key):
        # 进入添加页面
        self.epsAdd("电梯管制区域管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClick(fieldName="楼宇", labelName=Data['楼宇'])
        self.epsSelectClickContain(fieldName="机器人类型(默认为MIR)", labelName=Data['机器人类型(默认为MIR)'])
        self.epsWrite(fieldName="楼层 ", value=Data['楼层 '])
        self.epsWrite(fieldName="最大逗留时间(默认60秒) ", value=Data['最大逗留时间(默认60秒) '])
        self.epsWrite(fieldName="到等待位置的超时时间(默认300秒) ", value=Data['到等待位置的超时时间(默认300秒) '])
        self.epsSelectClick(fieldName="电梯管制区域内部交通管制类型 ", labelName=Data['电梯管制区域内部交通管制类型 '])
        self.epsWrite(fieldName="备注", value=Data['备注'])
        print(Data['备注'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ElevatorControlAreaManage_change(self):
        # 进入添加页面
        self.epsChange("电梯管制区域管理", '自动化测试脚本,自动化测试公司,A')
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        self.epsSelectClick(fieldName="机器人类型(默认为MIR)", labelName='Y1(云际)')
        self.epsWrite(fieldName="最大逗留时间(默认60秒) ", value="10")
        self.epsWrite(fieldName="到等待位置的超时时间(默认300秒) ", value='300')
        self.epsSelectClick(fieldName="电梯管制区域内部交通管制类型 ", labelName="不受交通管制的影响")
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ElevatorControlAreaManage_delete(self):
        # 点击删除
        self.epsDelete("电梯管制区域管理", '自动化测试脚本')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
