import time
import unittest
# from ddt import ddt, unpack, data
from Comm.BaseFrame.Base import Clear_environment
from Testcase.LogisticsAdminTest import logisticsInit


# 获取测试数据
# testCaseData = GetData(string_ele=r"name=电梯管理", TableName='/Data/LogisticsAdminTest/配件管理/配件管理.xlsx')
# print(testCaseData)


class FloorManage(logisticsInit):

    def test_FloorManage_add(self):
        self.epsAdd("楼层管理")
        self.epsSelectClick(fieldName='楼宇', labelName='A')
        self.epsWrite(fieldName='楼层', value='9')
        self.epsWriteContain(fieldName='数量', value='1')
        self.epsSelectClick(fieldName='是否是免打扰楼层', labelName='是')
        self.epsWriteContain(fieldName='音量', value='10')
        self.epsWrite(fieldName='备注', value='自动化测试脚本')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_FloorManage_change(self):
        # 进入修改页面
        self.epsChange('楼层管理', '自动化测试脚本')
        # 修改选项
        self.epsWrite(fieldName='楼层', value='5')
        self.epsWriteContain(fieldName='数量', value='2')
        self.epsSelectClick(fieldName='是否是免打扰楼层', labelName='否')
        self.epsWriteContain(fieldName='音量', value='100')
        self.epsWrite(fieldName='备注', value='修改自动化')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_FloorManage_delete(self):
        # 点击删除
        self.epsDelete("楼层管理", '修改自动化')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
