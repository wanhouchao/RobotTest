import unittest
# from ddt import ddt, unpack, data

# 获取测试数据
# testCaseData = GetData(string_ele=r"name=电梯管理", TableName='/Data/LogisticsAdminTest/配件管理/配件管理.xlsx')
# print(testCaseData)
from Testcase.LogisticsAdminTest import logisticsInit


class ControlSysManage(logisticsInit):

    def test_ControlSysManage_add(self):
        self.epsAdd("调度系统配置管理")
        self.epsSelectClickContain(fieldName='选择机器人型号', labelName='Y2(云际)')
        self.epsWriteContain(fieldName='安全距离', value='4')
        self.epsWriteContain(fieldName='危险距离', value='3')
        self.epsWriteContain(fieldName='不进行非管制区域检测的时间', value='7')
        self.epsClick(fieldName='是否启用充电调度', labelName='启用')
        self.epsWriteRemark(fieldName='备注信息', value='自动化测试脚本')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ControlSysManage_change(self):
        # 进入修改页面
        self.epsChange('调度系统配置管理', '自动化测试脚本')
        # 修改选项
        self.epsSelectClickContain(fieldName='选择机器人型号', labelName='Y1(云际)')
        self.epsWriteContain(fieldName='安全距离', value='10')
        self.epsWriteContain(fieldName='危险距离', value='10')
        self.epsWriteContain(fieldName='不进行非管制区域检测的时间', value='10')
        self.epsClick(fieldName='是否启用充电调度', labelName='不启用（默认）')
        self.epsWriteRemark(fieldName='备注信息', value='修改自动化')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ControlSysManage_delete(self):
        # 点击删除
        self.epsDelete("调度系统配置管理", '修改自动化')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
