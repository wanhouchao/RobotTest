from Comm.RobotWeb.UnittestWeb import unittestWeb


class logisticsInit(unittestWeb):
    """
    初始化操作
    """

    @classmethod
    def setUpClass(cls) -> None:
        cls.br = cls().login(browserPattern=True)

    @classmethod
    def tearDownClass(cls) -> None:
        cls().epsExitBrowser()

    def setUp(self) -> None:  # 前置条件
        self.clickPanel()

    def tearDown(self) -> None:  # 还原测试环境
        self.clickPanel()
        print('执行完成')
