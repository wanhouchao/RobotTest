import unittest
from ddt import ddt, data
from Comm.DataDrive.DataDriver import GetData
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=机器人用户管理", TableName='机器人管理')


@ddt
# 继承Unittest里面自定义的方法
class RobotUserManage(logisticsInit):

    @data(*testCaseData)
    def test_RobotUserManage_add(self, key):
        # 进入添加页面
        self.epsAdd("机器人用户管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName='用户名', value=Data['用户名'])
        self.epsWrite(fieldName='密码', value=Data['密码'])
        self.epsWrite(fieldName='呢称', value=Data['呢称'])
        self.epsWrite(fieldName='楼层', value=Data['楼层'])
        self.epsWrite(fieldName='排序', value=Data['排序'])
        self.epsSelectClick(fieldName='所属楼宇', labelName=Data['所属楼宇'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RobotUserManage_change(self):
        # 进入修改页面
        self.epsChange('机器人用户管理', "zdhcsgs_" + 'liujunj1988')
        # 修改选项
        self.epsWrite(fieldName='用户名', value='liujunj1987')
        self.epsWrite(fieldName='密码', value='3214')
        self.epsWrite(fieldName='呢称', value='3214')
        self.epsWrite(fieldName='楼层', value='9')
        self.epsWrite(fieldName='排序', value='9')
        self.epsSelectClick(fieldName='所属楼宇', labelName='A')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RobotUserManage_delete(self):
        # 点击删除
        self.epsDelete("机器人用户管理", 'liujunj1987')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
