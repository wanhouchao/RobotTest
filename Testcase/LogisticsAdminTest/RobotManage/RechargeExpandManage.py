import time
import unittest
from ddt import ddt, data

from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=充电位置扩展管理", TableName='机器人管理')


@ddt
# 继承Unittest里面自定义的方法
class RechargeExpandManage(logisticsInit):

    @data(*testCaseData)
    def test_RechargeExpandManage_add(self, key):
        # 进入添加页面
        self.epsAdd("充电位置扩展管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClickContain(fieldName='充电位置', labelName=Data['充电位置'])
        self.epsSelectClickContain(fieldName='机器人', labelName=Data['机器人'])
        self.epsWrite(fieldName="检测距离配置(米)", value=Data["检测距离配置(米)"])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RechargeExpandManage_change(self):
        # 进入修改页面
        self.epsChange('充电位置扩展管理', 'wanhc')
        # 修改选项
        self.epsSelectClickContain(fieldName='充电位置', labelName='A ---- 充电 ---- Y1 ---- 9F')
        self.epsSelectClickContain(fieldName='机器人', labelName='jinwei')
        self.epsWrite(fieldName="检测距离配置(米)", value='6')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RechargeExpandManage_delete(self):
        # 点击删除
        self.epsDelete("充电位置扩展管理", 'jinwei')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
