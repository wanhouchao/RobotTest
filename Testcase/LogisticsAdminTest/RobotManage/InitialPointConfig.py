import time
import unittest
from ddt import ddt, data

from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=初始点配置", TableName='机器人管理')


@ddt
# 继承Unittest里面自定义的方法
class InitialPointConfig(logisticsInit):


    @data(*testCaseData)
    def test_InitialPointConfig_add(self, key):
        # 进入添加页面
        self.epsAdd("初始点配置")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.TimeControl(fieldName='工作开始时间', value=Data['工作开始时间'])
        self.TimeControl(fieldName='工作结束时间', value=Data['工作结束时间'])
        self.epsSelectClick(fieldName='楼宇', labelName=Data['楼宇'])
        self.epsSelectClickContain(fieldName="选择机器人类型", labelName=Data["选择机器人类型"])
        self.epsSelectClickContain(fieldName="闲时回指定位置", labelName=Data["闲时回指定位置"])
        self.epsSelectClickContain(fieldName="机器人(选填)", labelName=Data["机器人(选填)"])
        self.epsWrite(fieldName="检测距离配置(米)", value=Data["检测距离配置(米)"])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_InitialPointConfig_change(self):
        # 进入修改页面
        self.epsChange('初始点配置', 'jinwei')
        # 修改选项
        self.TimeControl(fieldName='工作开始时间', value='17:01')
        self.TimeControl(fieldName='工作结束时间', value='18:02')
        self.epsSelectClick(fieldName='楼宇', labelName='A')
        self.epsSelectClickContain(fieldName="选择机器人类型", labelName='Y1(云际)')
        self.epsSelectClickContain(fieldName="闲时回指定位置", labelName='我的位置')
        self.epsSelectClickContain(fieldName="机器人(选填)", labelName='wanhc')
        self.epsWrite(fieldName="检测距离配置(米)", value='3')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_InitialPointConfig_delete(self):
        # 点击删除
        self.epsDelete("初始点配置", '我的位置')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
