# import time
# import unittest
# from ddt import ddt, unpack, data
#
# from Comm.Base import Clear_environment
# from Comm.DateDriver import GetData
# from Comm.SeleniumFramework import SeleniumFramework
# from Comm.UnittestFramework import Unittest
#
# # 获取测试数据
# from Config.test_epsit import Robot_url
#
# testCaseData = GetData(string_ele="name=初始点配置", TableName='机器人管理')
#
#
# @ddt
# # 继承Unittest里面自定义的方法
# class InitialPointConfig(Unittest):
#     @classmethod
#     def setUpClass(cls) -> None:
#         cls.br = cls().login()
#
#     @classmethod
#     def tearDownClass(cls) -> None:
#         Clear_environment()  # 测试用例跑完清理测试环境
#
#     def setUp(self) -> None:  # 前置条件
#         # Clear_environment()  # 清理web环境
#         self.br.onClick('xpath=//span[text()="面板"]/../div/i')
#         time.sleep(1)
#
#     def tearDown(self) -> None:  # 还原测试环境
#         time.sleep(1)
#         self.br.onClick('xpath=//span[text()="面板"]/../div/i')
#         print('执行完成')
#
#     def test_InitialPointConfig_change(self):
#         # 进入修改页面点击修改选项
#         # self.epsChange('楼层排序管理', '自动化测试公司,A,Y1,3')
#         eleInfo = {'所属机构': '自动化测试公司', '楼宇': 'A', '楼层': '3', '机器人类型': 'Y1',
#                    '排序': '3'}
#         self.clickLevelPositioningElement('楼层排序管理', '修改', eleInfo=eleInfo)
#         self.epsWrite(fieldName='排序', value='2')
#         # 添加页面提交
#         self.epsSubmit()
#         # 断言判断
#         self.doAssert()
#
#
# if __name__ == '__main__':
#     unittest.main()
