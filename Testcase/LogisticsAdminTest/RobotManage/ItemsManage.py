import time
import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=物品管理", TableName='机器人管理')


@ddt
class ItemsManage(logisticsInit):
    """
    物品管理
    """

    @data(*testCaseData)
    def test_ItemsManage_add(self, key):
        # 进入添加页面
        self.epsAdd('物品管理', fieldName="机构ID")
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName="物品编号", value=Data['物品编号'])
        self.epsWrite(fieldName="物品名称", value=Data['物品名称'])
        self.epsWrite(fieldName="规格类型", value=Data['规格类型'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ItemsManage_change(self):
        # 进入添加页面
        self.goToPage("物品管理")
        self.epsSearchClick('软件测试', placeholder='输入名称')
        self.epsWrite(fieldName="物品编号", value='2014')
        self.epsWrite(fieldName="物品名称", value='手机壳')
        self.epsWrite(fieldName="规格类型", value='12132')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_ItemsManage_delete(self):
        # 点击删除
        self.goToPage("物品管理")
        self.epsSearchClick('手机壳', action='删除', placeholder='输入名称')
        self.epsOnDelete()
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
