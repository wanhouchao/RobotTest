import time
import unittest
from ddt import ddt, data
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=语音提示管理", TableName='机器人管理')


@ddt
# 继承Unittest里面自定义的方法
class VoicePromptManage(logisticsInit):

    @data(*testCaseData)
    def test_VoicePromptManage_add(self, key):
        # 进入添加页面
        self.epsAdd("语音提示管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClickContain(fieldName='机器人', labelName=Data['机器人'])
        self.epsSelectClickContain(fieldName='语音提醒状态', labelName=Data['语音提醒状态'])
        self.epsWrite(fieldName="语音提醒文字", value=Data["语音提醒文字"])
        self.epsWrite(fieldName="提醒次数", value=Data["提醒次数"])
        self.epsWrite(fieldName="间隔时间(s)", value=Data["间隔时间(s)"])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_VoicePromptManage_change(self):
        # 进入修改页面
        self.epsChange('语音提示管理', 'wanhc')
        # 修改选项
        self.epsSelectClickContain(fieldName='机器人', labelName='jinwei')
        self.epsSelectClickContain(fieldName='语音提醒状态', labelName='去往消毒下一目的地播报')
        self.epsWrite(fieldName="语音提醒文字", value="自动化脚本测试1")
        self.epsWrite(fieldName="提醒次数", value='2')
        self.epsWrite(fieldName="间隔时间(s)", value='13')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断30
        self.doAssert()

    def test_VoicePromptManage_delete(self):
        # 点击删除
        self.epsDelete("语音提示管理", 'jinwei')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
