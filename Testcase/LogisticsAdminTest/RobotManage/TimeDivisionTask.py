import time
import unittest
from ddt import ddt, data

from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=分时段任务", TableName='机器人管理')


@ddt
# 继承Unittest里面自定义的方法
class TimeDivisionTask(logisticsInit):

    @data(*testCaseData)
    def test_TimeDivisionTask_add(self, key):
        # 进入添加页面
        self.epsAdd("分时段任务")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClickContain(fieldName="机器人", labelName=Data["机器人"])
        self.TimeControl(fieldName='开始时间', value=Data['开始时间'])
        self.TimeControl(fieldName='结束时间(支持跨天)', value=Data['结束时间(支持跨天)'])
        self.epsSelectClick(fieldName="检测状态(多选)", labelName=Data["检测状态(多选)"])
        self.epsSelectClickContain(fieldName='位置', labelName=Data['位置'])
        self.epsSelectClick(fieldName="任务类型", labelName=Data["任务类型"])
        self.epsSelectClick(fieldName="检测规则", labelName=Data["检测规则"])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_TimeDivisionTask_change(self):
        # 进入修改页面
        self.epsChange('分时段任务', 'wanhc')
        # 修改选项
        self.epsSelectClickContain(fieldName="机器人", labelName='jinwei')
        self.TimeControl(fieldName='开始时间', value='17:01')
        self.TimeControl(fieldName='结束时间(支持跨天)', value='18:02')
        self.epsSelectClick(fieldName='检测状态(多选)', labelName='配送中')
        self.epsSelectClickContain(fieldName="位置", labelName='A ---- 前台 ---- E2 ---- 9F')
        self.epsSelectClick(fieldName="任务类型", labelName='充电任务')
        self.epsSelectClick(fieldName="检测规则", labelName='检测一次')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_TimeDivisionTask_delete(self):
        # 点击删除
        self.epsDelete("分时段任务", 'jinwei')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
