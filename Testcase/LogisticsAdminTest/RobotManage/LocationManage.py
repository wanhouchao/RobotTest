# import time
# import unittest
# from ddt import ddt, unpack, data
#
# from Comm.Base import Clear_environment
# from Comm.DateDriver import GetData
# from Comm.SeleniumFramework import SeleniumFramework
# from Comm.UnittestFramework import Unittest
#
# # 获取测试数据
# from Config.test_epsit import Robot_url
#
# testCaseData = GetData(string_ele="name=位置管理", TableName='机器人管理')
#
#
# @ddt
# # 继承Unittest里面自定义的方法
# class LocationManage(Unittest):
#     @classmethod
#     def setUpClass(cls) -> None:
#         cls.br = cls().login()
#
#     @classmethod
#     def tearDownClass(cls) -> None:
#         Clear_environment()  # 测试用例跑完清理测试环境
#
#     def setUp(self) -> None:  # 前置条件
#         # Clear_environment()  # 清理web环境
#         self.clickPanel()  # 点击面板
#         time.sleep(1)
#
#     def tearDown(self) -> None:  # 还原测试环境
#         time.sleep(1)
#         self.clickPanel()  # 点击面板
#         print('执行完成')
#
#     @data(*testCaseData)
#     def test_LocationManage_add(self, key):
#         # 进入添加页面
#         self.epsAdd("位置管理")
#         # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
#         Data = testCaseData[key]  # 是一个字典
#         self.epsSelectClick(fieldName='楼宇', labelName=Data['楼宇'])
#         self.epsSelectClick(fieldName='关联地图', labelName=Data['关联地图'])
#         self.epsWrite(fieldName="位置名称", value=Data['位置名称'])
#         self.epsWrite(fieldName="位置类型", value=Data['位置类型'])
#         self.br.onClick('xpath=//span[text()="拼接ID"]/..')  # 点击拼接
#         self.epsWrite(fieldName="x", value=Data['x'])
#         self.epsWrite(fieldName="y", value=Data['y'])
#         self.epsWrite(fieldName="z", value=Data['z'])
#         self.epsWrite(fieldName="w", value=Data['w'])
#         self.epsWrite(fieldName="位置排序", value=Data['位置排序'])
#         self.epsWrite(fieldName="分机号", value=Data['分机号'])
#         # 添加页面提交
#         self.epsSubmit()
#         # 断言判断
#         self.doAssert()
#
#     def test_LocationManage_change(self):
#         # 进入修改页面
#         self.epsChange('位置管理', '自动化测试点')
#         # 修改选项
#         Data = {
#             '位置排序': '4',
#             '分机号': '131564654',
#             '楼宇': 'GG',
#         }
#         self.epsSelectClick(fieldName='楼宇', labelName=Data['楼宇'])
#         self.epsWrite(fieldName="位置排序", value=Data['位置排序'])
#         self.epsWrite(fieldName="分机号", value=Data['分机号'])
#
#         # 添加页面提交
#         self.epsSubmit()
#         # 断言判断
#         self.doAssert()
#
#     def test_LocationManage_delete(self):
#         # 点击删除
#         self.epsDelete('位置管理', '自动化测试点')
#         # 添加断言
#         self.doAssert()
#
#
# if __name__ == '__main__':
#     unittest.main()
