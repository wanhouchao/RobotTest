import time
import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=机器人部件管理", TableName='机器人管理')


@ddt
class RobotParts(logisticsInit):
    """
    机器人部件管理
    """

    @data(*testCaseData)
    def test_RobotParts_add(self, key):
        # 进入添加页面
        self.goToAddPage("机器人部件管理")
        Data = testCaseData[key]  # 是一个字典
        self.epsWrite(fieldName="编号", value=Data['编号'])
        self.epsWrite(fieldName="流量卡号", value=Data['流量卡号'])
        self.epsWrite(fieldName="wifi ssid", value=Data['wifi ssid'])
        self.epsWrite(fieldName="wifi密码", value=Data['wifi密码'])
        self.epsWrite(fieldName="路由器登录账号", value=Data['路由器登录账号'])
        self.epsWrite(fieldName="路由器登录密码", value=Data['路由器登录密码'])
        self.epsWrite(fieldName="底盘编号", value=Data['底盘编号'])
        self.epsWrite(fieldName="出厂批号", value=Data['出厂批号'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RobotParts_change(self):
        # 进入添加页面
        self.goToPage("机器人部件管理")
        self.epsSearchClick('2013', placeholder='输入编号')
        self.epsWrite(fieldName="编号", value='2014')
        self.epsWrite(fieldName="流量卡号", value='5465465465')
        self.epsWrite(fieldName="wifi ssid", value='213215616')
        self.epsWrite(fieldName="wifi密码", value='dasd565465')
        self.epsWrite(fieldName="路由器登录账号", value='ashdjh')
        self.epsWrite(fieldName="路由器登录密码", value='asjdjk132')
        self.epsWrite(fieldName="底盘编号", value='892116')
        self.epsWrite(fieldName="出厂批号", value='48913316')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RobotParts_delete(self):
        # 点击删除
        self.goToPage("机器人部件管理")
        self.epsSearchClick('2014', action='删除', placeholder='输入编号')
        self.epsOnDelete()
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
