import time
import unittest
from ddt import ddt, data

from Comm.BaseFrame.Base import Clear_environment
from Comm.DataDrive.DataDriver import GetData
from Comm.RobotWeb.UnittestWeb import unittestWeb

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=位置扩展管理", TableName='机器人管理')


@ddt
# 继承Unittest里面自定义的方法
class LocationExpandManage(logisticsInit):

    @data(*testCaseData)
    def test_LocationExpandManage_add(self, key):
        # 进入添加页面
        self.epsAdd("位置扩展管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClickContain(fieldName='对应位置', labelName=Data['对应位置'])
        self.epsWrite(fieldName="自定义位置编码", value=Data["自定义位置编码"])
        self.epsWrite(fieldName="自定义位置名称", value=Data["自定义位置名称"])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_LocationExpandManage_change(self):
        # 进入修改页面
        self.epsChange('位置扩展管理', '自动化测试')
        # 修改选项
        self.epsSelectClickContain(fieldName='对应位置', labelName='A ---- 前台 ---- E2 ---- 9F')
        self.epsWrite(fieldName="自定义位置编码", value='hello word')
        self.epsWrite(fieldName="自定义位置名称", value='自动化测试点')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_LocationExpandManage_delete(self):
        # 点击删除
        self.epsDelete("位置扩展管理", '自动化测试点')
        # 添加断言
        self.doAssert()


if __name__ == '__main__':
    unittest.main()
