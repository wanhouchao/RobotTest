import time
import unittest
from ddt import ddt, data
from Comm.BaseFrame.Base import Clear_environment
from Comm.RobotWeb.UnittestWeb import unittestWeb
from Comm.DataDrive.DataDriver import GetData

# 获取测试数据
from Testcase.LogisticsAdminTest import logisticsInit

testCaseData = GetData(string_ele="name=物流机器人管理", TableName='机器人管理')


# print(testCaseData)

@ddt
class RobotManage(logisticsInit):

    @data(*testCaseData)
    def test_RobotManage_add(self, key):
        # 进入添加页面
        self.epsAdd("物流机器人管理")
        # 加载数据"test_case1":{'所属机构': 1.0, '楼宇': 1.0, '机器人类型': 1.0}
        Data = testCaseData[key]  # 是一个字典
        self.epsSelectClick(fieldName='所属楼宇', labelName='A')
        self.epsWrite(fieldName='名称', value=Data['名称'])
        self.epsWrite(fieldName='序号', value=Data['序号'])
        self.epsSelectClick(fieldName='型号', labelName='Y2(云际)')
        self.epsWrite(fieldName='运行速度', value=Data['运行速度'])
        self.epsWrite(fieldName='登录账号', value=Data['登录账号'])
        self.epsWrite(fieldName='登录密码', value=Data['登录密码'])
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RobotManage_change(self):
        # 进入修改页面
        self.epsChange('物流机器人管理', 'wuyanzu1')
        # 修改选项
        self.epsWrite(fieldName='名称', value='pengyuyan')
        self.epsWrite(fieldName='登录账号', value='pengyuyan')
        # 添加页面提交
        self.epsSubmit()
        # 断言判断
        self.doAssert()

    def test_RobotManage_delete(self):
        # 点击删除
        self.epsDelete("物流机器人管理", 'pengyuyan')
        # 添加断言
        self.doAssert()


if __name__ == "__main__":
    unittest.main()
