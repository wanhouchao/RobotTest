import unittest
from Comm.BaseFrame.Log import MyLog
from Comm.RobotApi.UnittestApi import unittestApi
from Comm.TestCaseAssert.UnittestAssert import Assert


class addcnApiInit(unittestApi, Assert):
    """
    初始化操作
    """

    # @classmethod
    # def setUpClass(cls) -> None:
    #     cls.api = cls().webGetToken()
    #     MyLog.info("获取token成功")

    # @classmethod
    # def tearDownClass(cls) -> None:
    #     cls().epsExitBrowser()

    def setUp(self) -> None:  # 前置条件
        print("开始")
        self.api = unittestApi()

    def tearDown(self) -> None:  # 还原测试环境
        print('执行完成')

    # def test_registerAndAuth(self):
    #     """
    #     注册-->实名-->后台审核
    #     注册到审核的过程
    #     :return:
    #     """
    #     data, response = self.webUserRegister()
    #     mobile = data["mobile"]
    #     userId = response["data"]["user_id"]
    #     self.webApplyAuth(mobile=mobile)
    #     self.adminAuthAudit(userId=userId)
    #
    def test_test1(self):
        """调试代码"""
        # data, response = self.webUserRegister()
        # mobile = data["mobile"]
        # userId = response["data"]["user_id"]
        self.webApplyAuth(mobile="83365386")
        self.adminAuthAudit(userId="1084202")
        # self.webUserRegister()




if __name__ == '__main__':
    unittest.main()
