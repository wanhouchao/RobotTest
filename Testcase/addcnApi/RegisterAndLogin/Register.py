import unittest
from Comm.BaseFrame.Log import MyLog
from Comm.TestCaseAssert.UnittestAssert import Assert
from Testcase.addcnApi import addcnApiInit


class Register(addcnApiInit, Assert):
    """
    注册api
    """

    def test_mobileRegister(self):
        """
        通过手机号注册
        :return:
        """
        response = self.webUserRegister()
        print("接口返回参数-->{}".format(response))
        MyLog.info("接口返回参数-->{}".format(response))
        self.assertApiResponse(response=response, expectedResult={'status': 2, 'message': '登录成功', 'code': 200})


if __name__ == '__main__':
    unittest.main()
