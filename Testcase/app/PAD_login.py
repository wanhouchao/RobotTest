import unittest
import time
from Comm.RobotApp.Uiautomator import UiautomatorFrame


class Pad_login(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.dr = UiautomatorFrame()
        cls.dr.openApp()

    @classmethod
    def tearDownClass(cls):
        cls.dr = UiautomatorFrame()
        cls.dr.exitApp()


    def test_Login_001(self):
        time.sleep(5)
        self.dr.doWrite('resourceId=et_login_account','liujunjie',time=1)
        time.sleep(2)
        self.dr.doWrite('resourceId=et_login_password','4321',time=1)
        time.sleep(2)
        self.dr.onClick('text=登录')
        time.sleep(5)
        # self.dr.Assert('text=y2p02','y2p02')


if __name__ == '__main__':
    unittest.main()