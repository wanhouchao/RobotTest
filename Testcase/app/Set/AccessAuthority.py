import unittest
from Comm.RobotApp.UnittestApp import unittestApp
from Config.AppConfig.AppConfig import packageName


class AccessAuthority(unittestApp):

    def setUp(self) -> None:
        self.dr = self.loginApp()

    def tearDown(self) -> None:
        self.dr.exitApp(packageName)

    def test_app_AccessAuthority(self):
        self.goSet()
        self.idVerify()
        print('启动成功')


if __name__ == '__main__':
    unittest.main()
