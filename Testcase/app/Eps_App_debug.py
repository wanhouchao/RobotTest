import unittest
from Comm.RobotApp.UnittestApp import unittestApp
from Config.AppConfig.AppConfig import packageName


class App_debug(unittestApp):

    # @classmethod
    # def setUpClass(cls):
    #     cls.dr = cls().loginApp()
    #
    # @classmethod
    # def tearDownClass(cls):
    #     cls.dr = UiAutomatorFrame(devicesName)
    #     cls.dr.exitApp(packageName)

    def setUp(self) -> None:
        self.dr = self.loginApp()

    def tearDown(self) -> None:
        self.dr.exitApp(packageName)

    # def Authentication(self):  # 身份验证
    #     self.dr.onClick('resourceId=iv_user_item')
    #     self.dr.onClick('text=1')
    #     self.dr.onClick('text=2')
    #     self.dr.onClick('text=3')
    #     self.dr.onClick('text=4')
    #
    # def Distribution(self):  # 配送流程
    #     self.dr.onClick('resourceId=iv_home_transport' )
    #     self.Authentication()
    #     self.dr.onClick('text=走廊4')
    #     self.dr.onClick('resourceId=sb_go')
    #     self.dr.onClick('text=接收物品')
    #     self.Authentication()
    #     self.dr.onClick('text=完成')
    #
    # def Back_tracking(self):  # 返程流程
    #     self.dr.onClick('resourceId=iv_home_guide')
    #     self.dr.onClick('text=返程')
    #     self.Authentication()
    #
    # def Charge(self):
    #     self.dr.onClick('resourceId=iv_home_guide')
    #     self.dr.onClick('text=充电')
    #     self.Authentication()
    #
    # def test_App_debug1(self):
    #     self.Distribution()
    #     time.sleep(2)
    #
    # def test_App_debug2(self):
    #     self.Back_tracking()
    #     time.sleep(2)
    #
    # def test_App_debug3(self):
    #     self.Charge()
    #     time.sleep(2)

    def test_app_login(self):

        # iv_home_transport
        # self.escTask()
        # self.appDriver.onClick('xpath=//*[@r')
        self.goSet()
        self.functionSwitch(acoustoOptic=False)
        # self.waitTime(waitTobe="2:1")
        print('启动成功')


if __name__ == '__main__':
    unittest.main()
