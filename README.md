# RobotTest

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构 | Architecture
这个是一个python项目主要使用的是python3里面的的request，selenium，appium，uiautomator2等第三方库Windows环境下可以用run.bat来启动它查看演示。

* ChromeDriver主要存放浏览器驱动目前是只有chromedriver，这个里面的驱动文件是代码自动下载解压的
* Comm主要是存放公共方法文件夹，包括了api,app,web,log,mysql,和一些其他的二次开发的方法
* Config主要是存放相关配置文件
* Data主要是存放测试数据的文件，是用来做数据驱动的
* Log包含了当前脚本运行的日志记录，目前是只保留了两天的日志
* OtherScript主要是存放了一些调试脚本和一些探索性的草稿
* Report主要是存放脚本生成的报告
* RunMain主要是存放用例执行文件
* TestCase主要是放置了一些用例


#### 安装教程

1. pip install -r environment.txt

#### 使用说明
demo可以直接运行RunCase.py，在report查看报告

#### 部署

1.  执行测试脚本的时候回先判断当前运行的环境是windows还是Linux

* Windows下载谷歌浏览器和相关谷歌驱动
* Linux安装谷歌浏览和相关驱动，再给谷歌驱动给权限 一般都是:
```shell script
chmod 777 谷歌驱动路径
```

2.  pip安装相关的库
3.  测试框架是否部署成功
```shell script
cd RobotTest/ 切换到项目目录
python  runDemo.py 运行demo脚本
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
